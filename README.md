# Minno

Minno is a game where you start with no inventory.
Not just with an empty inventory but without the inventory itself.
In order to pick items up and move through the levels you must find inventory pieces and build your inventory.

This turns the game into a spatial puzzle all about packing items into your limited inventory.

### Controls

* `w`/`a`/`s`/`d`, `z`/`q`/`s`/`d`, or the arrow keys to move
* `e`, `i` or `<Escape>` enters and leaves inventory selection mode
* `<Space>`, or `<Enter>` picks up and places inventory items
* `r` restarts the current level


### Building

Build the program with:

    elm make src/Main.elm --output elm.js
