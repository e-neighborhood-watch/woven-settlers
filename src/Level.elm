module Level exposing
  ( Level
  , Tile
    ( ..
    )
  , Link
  , inBounds
  , width
  , height
  , tileAt
  , setTileAt
  , updateDoors
  )

import Mino exposing
  ( Mino
  )
import Basics2 exposing
  ( mapAt
  )
import Inventory exposing
  ( Inventory
  , occupiedSpaces
  )

import Array exposing
  ( Array
  )

type Tile
  = Wall
  | Win
  | Empty
  | Item Mino Int
  | NewSlots Mino
  | Bin (Inventory {})
  | Pack (Inventory {})
  | Door Bool

type alias Link =
  { from : (Int, Int)
  , to   : (Int, Int)
  }

type alias Level =
  { tiles : Array (Array Tile)
  , links : List Link
  }

emptyAt : (Int, Int) -> Level -> Level
emptyAt = setTileAt Empty

setTileAt : Tile -> (Int, Int) -> Level -> Level
setTileAt tile (x, y) lvl =
  { lvl
  | tiles =
    mapAt y (Array.set x tile) lvl.tiles
  }

tileAt : Level -> (Int, Int) -> Tile
tileAt lvl (x, y) =
  case
    Array.get y lvl.tiles
     |> Maybe.andThen (Array.get x)
  of
    -- Going out of bounds wins
    Nothing    -> Win
    Just thing -> thing

updateDoors : Level -> Level
updateDoors level =
  let
    checkLink : Link -> Level -> Level
    checkLink { to, from } lvl =
      case
        tileAt lvl from
      of
         Bin inv ->
           if
             occupiedSpaces inv == inv.footprint
           then
             setTileAt (Door False) to lvl
           else
             setTileAt (Door True) to lvl
         Pack inv ->
           if
             occupiedSpaces inv == inv.footprint
           then
             setTileAt (Door False) to lvl
           else
             setTileAt (Door True) to lvl
         _ ->
           -- Uh oh this level was not properly formatted
           lvl
  in
    List.foldl checkLink level level.links

inBounds : Level -> (Int, Int) -> Bool
inBounds lvl (x, y) =
  case tileAt lvl (x, y) of
    Wall ->
      False
    _ ->
      True

width : Level -> Int
width =
  .tiles
    >> Array.map Array.length
      >> Array.foldl max 0

height : Level -> Int
height = .tiles >> Array.length

