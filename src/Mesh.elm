module Mesh exposing
  ( Vertex
  , buildBlockMesh
  , buildWallsMesh
  , buildItemsMesh
  , buildNewSlotsMesh
  , buildBinsMesh
  , buildBinItemsMesh
  , buildInventoryOutlineMesh
  , buildHeldNewSlotMesh
  , buildInventoryPiecesMesh
  , buildHeldItemMesh
  , buildCursorMesh
  , buildInventoryGhostMesh
  , buildCompletedLinksMesh
  , buildUncompletedLinksMesh
  , buildOpenDoorsMesh
  )

import Animation exposing
  ( Moving
  , Animated
  , Labeled
  , AnimationLabel (..)
  , moveAnimationOffset
  )
import Basics2 exposing (..)
import Color exposing (createUniqueColor)
import Holding exposing (..)
import Inventory exposing
  ( Inventory
  , Region
  , Colored
  , toLocations
  )
import Level exposing (..)
import Mino exposing (Mino)
import Model exposing (PrgmModel)
import Movement exposing (..)

import Records exposing (..)
import Array exposing (Array)
import Set exposing (Set)
import WebGL exposing (Mesh) --, Shader, alpha, antialias)
import Math.Vector2 as Vec2 exposing (vec2, Vec2, add)
import Math.Vector4 as Vec4 exposing (vec4, Vec4)

type alias Triangle a = (a, a, a)
type alias Triangles a = List (Triangle a)

type alias Vertex = Vertexed { }

shiftTriangles : Vec2 -> Triangles { a | abs_position : Vec2 } -> Triangles { a | abs_position : Vec2 }
shiftTriangles shift =
  let
    shiftTriangle : Triangle { a | abs_position : Vec2 } -> Triangle { a | abs_position : Vec2 }
    shiftTriangle (v1, v2, v3) =
      ( { v1
        | abs_position = add v1.abs_position shift
        }
      , { v2
        | abs_position = add v2.abs_position shift
        }
      , { v3
        | abs_position = add v3.abs_position shift
        }
      )
  in
    List.map
      shiftTriangle

-- Should likely take Vec2
buildUnitSquare : Vec4 -> Vec2 -> Triangles Vertex
buildUnitSquare color llCorner =
  square llCorner 1.0 color

buildCenteredSquare : Float -> Vec4 -> Vec2 -> Triangles Vertex
buildCenteredSquare size color llCorner =
  let
    w = (1.0 - size) / 2.0
  in
    square (vec2 w w |> add llCorner) size color

buildSmallSquare : Vec4 -> Vec2 -> Triangles Vertex
buildSmallSquare = buildCenteredSquare (1.0 / 3.0)

buildBlockMesh :
  (
    Bool
  ->
    { a
    | blockLocation : (Int, Int)
    , currentMoveAnimation : Maybe (Moving (Animated b))
    , currentScaleAnimation : Maybe (Labeled (Animated b))
    }
  ->
    Mesh Vertex
  )
buildBlockMesh colorOn { blockLocation, currentMoveAnimation, currentScaleAnimation } =
  let
    moveAdjustment =
      case currentMoveAnimation of
        Just mAnimation ->
          case mAnimation.destinationHeldItem of
            Playing ->
              moveAnimationOffset blockLocation mAnimation
            _ ->
              vec2 0.0 0.0
        _ ->
          vec2 0.0 0.0
    finalAdjustment =
      case currentScaleAnimation of
        Just sAnimation ->
          case sAnimation.label of
            Impulse movement _ ->
              let
                displacement = 0.25 * sin ( sAnimation.fractionLeft * pi )
              in
                case movement of
                  MoveUp ->
                    vec2 0 -displacement
                  MoveLeft ->
                    vec2 -displacement 0
                  MoveDown ->
                    vec2 0 displacement
                  MoveRight ->
                    vec2 displacement 0
                  _ ->
                    moveAdjustment
            _ ->
              moveAdjustment
        _ ->
          moveAdjustment
    blockColor =
      if
        colorOn
      then
        vec4 0.8 0.2 0.2 1.0
      else
        vec4 0.5 0.5 0.5 1.0
  in
    buildUnitSquare blockColor (add finalAdjustment <| makeVec2 blockLocation)
        |> WebGL.triangles

buildWallsMesh : Level -> Mesh Vertex
buildWallsMesh { tiles } =
  let
    intZip : (Int -> a -> List b) -> Array a -> List b
    intZip f a =
      Array.toIndexedList a
        |> List.map (uncurry f)
          |> List.concat
    join : Int -> Int -> Tile -> Triangles Vertex
    join y x a =
      case a of
        Wall ->
          buildUnitSquare (vec4 0.15 0.15 0.15 1.0) <| makeVec2 (x, y)
        Door True ->
          buildUnitSquare (vec4 0.75 0.75 0.1 1.0) <| makeVec2 (x, y)
        _ ->
          []
  in
    intZip (join >> intZip) tiles |> WebGL.triangles

buildOpenDoorsMesh : Level -> Mesh Vertex
buildOpenDoorsMesh { tiles } =
  let
    intZip : (Int -> a -> List b) -> Array a -> List b
    intZip f a =
      Array.toIndexedList a
        |> List.map (uncurry f)
          |> List.concat
    join : Int -> Int -> Tile -> Triangles Vertex
    join y x a =
      case a of
        Door False ->
          buildUnitSquare (vec4 0.75 0.75 0.1 1.0) <| makeVec2 (x, y)
        _ ->
          []
  in
    intZip (join >> intZip) tiles |> WebGL.triangles

buildItem : Mino -> Int -> Int -> Triangles Vertex
buildItem mino =
  let
    minoWidth = Mino.width mino
    minoHeight = Mino.height mino
    minoTiles = Mino.tiles mino
    maxMinoDimension = 
      if
        minoTiles < 5
      then
        max minoWidth minoHeight |> max minoTiles
      else
        max minoWidth minoHeight
  in
    buildSubGrid (vec4 0.25 0.75 0.3 1.0) maxMinoDimension mino

buildBin :
  (
    Vec4
  ->
    Mino
  ->
    Int
  ->
    Int
  ->
    Triangles Vertex
  )
buildBin binColor mino =
  let
    minoWidth = Mino.width mino
    minoHeight = Mino.height mino
    maxMinoDimension = max minoWidth minoHeight
  in
    buildSubGrid binColor maxMinoDimension mino

buildBinItems :
  (
    Vec4          -- color og the grid
  ->
    Inventory {}  -- inventory of the bin
  ->                           
    Int           -- x of the sub-grid
  ->                                  
    Int           -- y of the sub-grid
  ->
    Triangles Vertex
  )
buildBinItems subGridColor { footprint, heldItems} intx inty =
  let
    footprintWidth = Mino.width footprint
    footprintHeight = Mino.height footprint
    maxMinoDimension = max footprintWidth footprintHeight
    globalSideLength = 0.9 / (maxMinoDimension |> toFloat)

    center = vec2 (toFloat intx + 0.5) (toFloat inty + 0.5)

    createLocalCoordinate : (Int, Int) -> Vec2
    createLocalCoordinate (x, y) =
      vec2 
        (toFloat x - toFloat footprintWidth  / 2.0)
        (toFloat y - toFloat footprintHeight / 2.0)

    embellishedSquare : Vec2 -> Triangles Vertex
    embellishedSquare pos =
      square pos globalSideLength subGridColor
  in
    List.concatMap (toLocations >> Set.toList) heldItems
      |> List.map createLocalCoordinate
        |> List.map (Vec2.scale globalSideLength >> add center)
          |> List.concatMap embellishedSquare

buildSubGrid :
  (
    Vec4  -- Color of grid
  ->
    Int   -- Max dimension
  ->
    Mino  -- The mino to draw
  ->
    Int   -- x of the sub-grid
  ->
    Int   -- y of the sub-grid
  ->
    Triangles Vertex
  )
buildSubGrid
  subGridColor
  maxMinoDimension
  mino
  intx
  inty =
  let
    minoWidth = Mino.width mino |> toFloat
    minoHeight = Mino.height mino |> toFloat
    globalSideLength = 0.9 / (maxMinoDimension |> toFloat)

    center = vec2 (toFloat intx + 0.5) (toFloat inty + 0.5)
    finalScaleFactor = globalSideLength / 2

    createLocalCoordinate : (Int, Int) -> Vec2
    createLocalCoordinate (x,y) =
      vec2 
        (2 * toFloat x - minoWidth)
        (2 * toFloat y - minoHeight)


    embellishedSquare : Vec2 -> Triangles Vertex
    embellishedSquare pos =
      square pos globalSideLength subGridColor
  in
    mino
      |> Set.toList
        |> List.map createLocalCoordinate
          |> List.map (Vec2.scale finalScaleFactor >> add center)
            |> List.concatMap embellishedSquare

buildItemsMesh : Level -> Mesh Vertex
buildItemsMesh { tiles } =
  let
    intZip : (Int -> a -> List b) -> Array a -> List b
    intZip f a =
      Array.toIndexedList a
        |> List.map (uncurry f)
          |> List.concat
    join : Int -> Int -> Tile -> Triangles Vertex
    join y x a =
      case a of
        Item mino colorIndex ->
          buildItem mino x y
        _ ->
          []
  in
    intZip (join >> intZip) tiles |> WebGL.triangles

buildNewSlotsMesh : Level -> Mesh Vertex
buildNewSlotsMesh { tiles } =
  let
    intZip : (Int -> a -> List b) -> Array a -> List b
    intZip f a =
      Array.toIndexedList a
        |> List.map (uncurry f)
          |> List.concat
    join : Int -> Int -> Tile -> Triangles Vertex
    join y x a =
      case a of
        NewSlots mino ->
          buildBin (vec4 0.25 0.75 0.25 1.0) mino x y
        _ ->
          []
  in
    intZip (join >> intZip) tiles |> WebGL.triangles

buildBinsMesh :
  (
    Vec4
  ->
    Vec4
  ->
    Vec4
  ->
    { x
    | currentLevel : Level
    , blockLocation : (Int, Int)
    , currentMoveAnimation : Maybe (Moving (Animated y))
    }
  ->
    Mesh Vertex
  )
buildBinsMesh binColor lockColor packColor {currentLevel, blockLocation, currentMoveAnimation} =
  let
    intZip : (Int -> a -> List b) -> Array a -> List b
    intZip f a =
      Array.toIndexedList a
        |> List.map (uncurry f)
          |> List.concat

    renderInv : Vec4 -> (Int, Int) -> Inventory a -> Triangles Vertex
    renderInv color binLocation inv =
      if
        binLocation == blockLocation
          && (currentMoveAnimation |> Maybe.map (.destinationHeldItem >> (/=) Playing) |> Maybe.withDefault True)
      then
        -- Do not render, the block is on top
        [ ]
      else
        uncurry (buildBin color inv.footprint) binLocation

    join : Int -> Int -> Tile -> Triangles Vertex
    join y x a =
      case a of
        Bin inv ->
          renderInv binColor (x, y) inv
        Pack inv ->
          renderInv packColor (x, y) inv
        _ ->
          []
  in
    intZip (join >> intZip) currentLevel.tiles |> WebGL.triangles

buildBinItemsMesh :
  (
    Vec4
  ->
    Vec4
  ->
    Vec4
  ->
    { x
    | currentLevel : Level
    , blockLocation : (Int, Int)
    , currentMoveAnimation : Maybe (Moving (Animated y))
    }
  ->
    Mesh Vertex
  )
buildBinItemsMesh binColor lockColor packColor {currentLevel, blockLocation, currentMoveAnimation} =
  let
    intZip : (Int -> a -> List b) -> Array a -> List b
    intZip f a =
      Array.toIndexedList a
        |> List.map (uncurry f)
          |> List.concat

    buildInv : Vec4 -> (Int, Int) -> Inventory {} -> Triangles Vertex
    buildInv color location inventory =
      if
        location == blockLocation
          && (currentMoveAnimation |> Maybe.map (.destinationHeldItem >> (/=) Playing) |> Maybe.withDefault True)
      then
        -- Do not render if the block is on top
        [ ]
      else
        uncurry (buildBinItems color inventory) location

    join : Int -> Int -> Tile -> Triangles Vertex
    join y x a =
      case a of
        Bin inv ->
          buildInv binColor (x, y) inv
        Pack inv ->
          buildInv packColor (x, y) inv
        _ ->
          [ ]
  in
    intZip (join >> intZip) currentLevel.tiles |> WebGL.triangles
  

buildInventoryOutlineMesh :
  (
    Vec4
  ->
    (Int, Int)
  ->
    { a | footprint : Mino }
  ->
    Mesh Vertex
  )
buildInventoryOutlineMesh color llCorner { footprint }=
  { footprint =
    footprint
  , llCorner  =
    llCorner
  }
    |> toLocations
      |> Set.toList
        |> List.concatMap (makeVec2 >> buildUnitSquare color)
          |> WebGL.triangles

buildHeldNewSlotMesh : (Int, Int) -> Holding -> Mesh Vertex
buildHeldNewSlotMesh offset holding =
  case holding of
    HoldingNewSlot { footprint, llCorner } ->
      { footprint =
        footprint
      , llCorner  =
        tuplePlus offset llCorner
      }
        |> toLocations
          |> Set.toList
            |> List.concatMap
              ( makeVec2
              >> buildCenteredSquare (1/3.0) (vec4 0.1 0.9 0.1 1.0)
              )
                |> WebGL.triangles
    _ ->
      [] |> WebGL.triangles

buildInventoryPiecesMesh : PrgmModel -> (Int, Int) -> Inventory a -> Mesh Vertex
buildInventoryPiecesMesh model offset inventory=
  let
    buildMino : Colored (Region a) -> Triangles Vertex
    buildMino mino =
      let
        minoColor =
          case model.heldItem of
            -- Grey out if we are holding a mino
            HoldingMino _ ->
              vec4 0.4 0.4 0.4 1.0
            _ ->
              createUniqueColor mino.colorIndex
        locations = 
          { mino
          | llCorner =
            tuplePlus mino.llCorner offset
          } |> toLocations
      in
        locations |> Set.toList |>
          case
            model.currentScaleAnimation
          of
            Nothing ->
              List.concatMap
                <| makeVec2 >> buildUnitSquare minoColor
            Just sAnimation ->
              case 
                sAnimation.label
              of
                DropItem loc ->
                  if
                    Set.member loc locations
                  then
                    let
                      size = 1 - sAnimation.fractionLeft * (2/3.0)
                    in
                      List.concatMap
                        <| makeVec2 >> (buildCenteredSquare size minoColor)
                  else
                    List.concatMap
                      <| makeVec2 >> buildUnitSquare minoColor
                _ ->
                  List.concatMap
                    <| makeVec2 >> buildUnitSquare minoColor
  in
    inventory.heldItems
      |> List.concatMap
        buildMino
         |> WebGL.triangles

buildInventoryGhostMesh : Maybe (Labeled (Animated a)) -> Mesh Vertex
buildInventoryGhostMesh maybeScaleAnimation =
  WebGL.triangles <|
    case
      Maybe.map .label maybeScaleAnimation
    of
      Just (Impulse _ mino) ->
        let
          minoColor = vec4 0.6 0.1 0.1 1.0
          locations = 
            { footprint = mino
            , llCorner = (0, 0)
            } |> toLocations
          squareSize =
            case maybeScaleAnimation of
              Just { fractionLeft } ->
                1/3.0 + 2/3.0 * sin (fractionLeft * pi)
              Nothing -> 0
        in
          locations
            |> Set.toList
              |>
                ( List.concatMap
                <| makeVec2 >> buildCenteredSquare squareSize minoColor
                )
      _ ->
        [ ]

buildHeldItemMesh :
  (
    Holding
  ->
    Maybe (Moving (Animated a))
  ->
    Maybe (Labeled (Animated b))
  ->
    Mesh Vertex
  )
buildHeldItemMesh heldItem maybeMoveAnimation maybeScaleAnimation =
  let
    squareBuilder =
      case maybeScaleAnimation of
        Just scaleAnimation ->
          case scaleAnimation.label of
            PickUpItem ->
              buildCenteredSquare (1/3.0 + scaleAnimation.fractionLeft * (2/3.0))
            _ ->
              buildSmallSquare
        Nothing ->
          buildSmallSquare
  in
    WebGL.triangles
      <| case heldItem of
        HoldingMino piece ->
          toLocations piece
            |> Set.toList
              |>
                ( List.concatMap
                   ( ( squareBuilder
                     <| createUniqueColor piece.colorIndex
                     )
                   << makeVec2
                   )
                )
                |> case maybeMoveAnimation of
                  Just currentMoveAnimation ->
                    if
                      heldItem == currentMoveAnimation.destinationHeldItem
                    then
                      moveAnimationOffset piece.llCorner currentMoveAnimation
                        |> shiftTriangles
                    else
                      identity
                  _ ->
                    identity
        _ ->
          [ ]

buildCursorMesh : Holding -> Maybe (Moving (Animated a)) -> Mesh Vertex
buildCursorMesh heldItem currentMoveAnimation =
  WebGL.triangles
    <| case heldItem of
      Selecting loc ->
        buildSmallSquare
          (vec4 0.8 0.8 0.8 1.0)
          ( add
            ( currentMoveAnimation
              |> Maybe.map (moveAnimationOffset loc)
                |> Maybe.withDefault (vec2 0 0)
            )
            (makeVec2 loc)
          )
      _ ->
       [ ]

buildUncompletedLinksMesh : List (Link, Float) -> Mesh Vertex
buildUncompletedLinksMesh =
  let
    buildUnfilledLink : Link -> Float -> Triangles Vertex
    buildUnfilledLink link fractionComplete =
      let
        allLinkSquares = buildLinkSquares link
      in
        allLinkSquares
          |> List.drop (toFloat (List.length allLinkSquares) * fractionComplete |> ceiling)
            |> List.concat
  in
    List.concatMap (uncurry buildUnfilledLink) >> WebGL.triangles

buildCompletedLinksMesh : List (Link, Float) -> Mesh Vertex
buildCompletedLinksMesh =
  let
    buildFilledLink : Link -> Float -> Triangles Vertex
    buildFilledLink link fractionComplete =
      let
        allLinkSquares = buildLinkSquares link
      in
        allLinkSquares
          |> List.take (toFloat (List.length allLinkSquares) * fractionComplete |> ceiling)
            |> List.concat
  in
    List.concatMap (uncurry buildFilledLink) >> WebGL.triangles

buildLinkSquares : Link -> List (Triangles Vertex)
buildLinkSquares link =
  let
    numSquares = 4
    biRange : Int -> Int -> List Int
    biRange first last =
      if
        first <= last
      then
        List.range first last
      else
        List.range last first |> List.reverse
    (fromX, fromY) = link.from
    (toX, toY) = link.to
    xDirection = if toX > fromX then 1 else -1
    yDirection = if toY > fromY then 1 else -1
    horizontals =
      if abs (fromX - toX) <= 1
      then [ ]
      else biRange (fromX + xDirection) (toX - xDirection) |> List.map (flip Tuple.pair fromY)
    corner = if fromX == toX || fromY == toY then [ ] else [(toX, fromY)]
    verticals =
      if abs (fromY - toY) <= 1
      then [ ]
      else biRange (fromY + yDirection) (toY - yDirection) |> List.map (Tuple.pair toX)
    halfEmptySpaceSize = 0.5 - 1/(2*numSquares)
    buildHorizontalSquares : (Int, Int) -> List (Triangles Vertex)
    buildHorizontalSquares (intx, inty) =
      let
        x = toFloat intx
        y = toFloat inty
        buildHorizontalSquare : Int -> Triangles Vertex
        buildHorizontalSquare n =
          square (vec2 (x + toFloat n/numSquares) (y + (1 + yDirection)*halfEmptySpaceSize)) (1/numSquares) (vec4 0.75 0.75 0.1 1)
      in
        List.range 0 (numSquares - 1) |> List.map buildHorizontalSquare
          |> if
            fromX < toX
          then
            identity
          else
            List.reverse
    buildCornerSquares : (Int, Int) -> List (Triangles Vertex)
    buildCornerSquares loc =
      buildHorizontalSquares loc |> List.take 1
    buildVerticalSquares : (Int, Int) -> List (Triangles Vertex)
    buildVerticalSquares (intx, inty) =
      let
        x = toFloat intx
        y = toFloat inty
        buildVerticalSquare : Int -> Triangles Vertex
        buildVerticalSquare n =
          square (vec2 (x + (1 - xDirection)*halfEmptySpaceSize) (y + toFloat n/numSquares)) (1/numSquares) (vec4 0.75 0.75 0.1 1)
      in
        List.range 0 (numSquares - 1) |> List.map buildVerticalSquare
          |> if
            fromY < toY
          then
            identity
          else
            List.reverse
  in
    List.concatMap buildHorizontalSquares horizontals
      ++ List.concatMap buildCornerSquares corner
        ++ List.concatMap buildVerticalSquares verticals

square : Vec2 -> Float -> Vec4 -> Triangles Vertex
square topLeft side color =
  let
    vertex absPos relPos = { coloration = color, abs_position =  absPos, rel_position = relPos }
  in
    [ ( vertex topLeft (vec2 0.0 0.0)
      , vertex (add topLeft (vec2 side 0)) (vec2 1.0 0.0)
      , vertex (add topLeft (vec2 side side)) (vec2 1.0 1.0)
      )
    , ( vertex topLeft (vec2 0.0 0.0)
      , vertex (add topLeft (vec2 0 side)) (vec2 0.0 1.0)
      , vertex (add topLeft (vec2 side side)) (vec2 1.0 1.0)
      )
    ]
