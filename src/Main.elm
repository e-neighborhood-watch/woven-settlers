module Main exposing (main)

-- Internal imports
import Animation exposing (..)
import Basics2 exposing (..)
import Holding exposing (..)
import Level exposing (..)
import LevelData exposing (LevelData)
import Mesh
import Message exposing (..)
import Movement exposing (..)
import Model exposing (..)
import Mino exposing (Mino)
import Inventory exposing
  ( Inventory
  , canAdd
  , mergeSlots
  , toLocations
  )
import Levels.Sequence as LevelSequence
import View

-- External imports
import Array
  exposing
    ( Array
    )
import Browser
import Browser.Events
  exposing
    ( onAnimationFrameDelta
    , onKeyPress
    , onKeyDown
    )
import Html
  exposing
    ( Html
    , div
    )
import Set exposing (Set)
import Math.Vector2 as Vec2 exposing (vec2, Vec2, add)
import Math.Vector3 as Vec3 exposing (vec3, Vec3)
import Math.Vector4 as Vec4 exposing (vec4, Vec4)
import Maybe
import Json.Decode as Decode
import Json.Encode
import Platform.Sub as Sub

-- Main function

main : Program () PrgmModel PrgmMsg
main =
  Browser.element
    { init = init
    , view = View.view
    , subscriptions = subscriptions
    , update = update
    }

initLevel : CoreData a -> LevelData -> PrgmModel
initLevel coreData levelData =
  { blockLocation = levelData.blockLocation
  , currentLevel = levelData.currentLevel
  , elapsedTime = coreData.elapsedTime
  , blinkStart = coreData.blinkStart
  , upcomingLevels = coreData.upcomingLevels
  , playerInventory = Inventory.empty
  , heldItem = Playing
  , startOfLevel = levelData
  , currentMoveAnimation = Nothing
  , currentScaleAnimation = Nothing
  }

init : () -> ( PrgmModel, Cmd PrgmMsg )
init () =
  ( initLevel
    { elapsedTime =
      0.0
    , blinkStart =
      0.0
    , upcomingLevels =
      LevelSequence.tailLevels
    }
    LevelSequence.headLevel
  , Cmd.none
  )

subscriptions : PrgmModel -> Sub PrgmMsg
subscriptions {} =
  Sub.batch
    [ Decode.field "key" Decode.string
      |> Decode.map (toMovement >> UserMovement)
        |> onKeyDown
    , onAnimationFrameDelta TimeHappens
    ]

update : PrgmMsg -> PrgmModel -> ( PrgmModel, Cmd PrgmMsg )
update message model =
  ( case message of
    TimeHappens elapsed ->
      { model
      | elapsedTime =
        model.elapsedTime + elapsed
      , currentMoveAnimation =
        model.currentMoveAnimation
          |> Maybe.andThen (advanceAnimation elapsed)
      , currentScaleAnimation =
        model.currentScaleAnimation
          |> Maybe.andThen (advanceAnimation elapsed)
      }
    UserMovement RestartLevel ->
      initLevel
        model
        model.startOfLevel
    UserMovement move ->
      let
        effectiveInventory =
          case
            Level.tileAt model.currentLevel model.blockLocation
          of
            Bin inv2 ->
              Inventory.join model.playerInventory inv2
            _ ->
              model.playerInventory
      in
        case model.heldItem of
          HoldingNewSlot heldSlots ->
            case move of
              Select ->
                { model
                | playerInventory =
                  mergeSlots
                    heldSlots
                    model.playerInventory
                -- The item should no longer be held
                , heldItem =
                  Playing
                }
              _ ->
                { model
                | heldItem =
                  let
                    (x, y) = heldSlots.llCorner
                    (nextX, nextY) =
                      case move of
                        MoveUp    ->
                          (x, y-1)
                        MoveLeft  ->
                          (x-1, y)
                        MoveDown  ->
                          (x, y+1)
                        MoveRight ->
                          (x+1, y)
                        _         ->
                          (x, y)
                    nextLocation =
                      ( nextX
                      , nextY
                      )
                  in
                    HoldingNewSlot
                      { heldSlots
                      | llCorner =
                        nextLocation
                      }
                }
          HoldingMino heldMino ->
            case move of
              Select ->
                if
                  canAdd heldMino model.playerInventory
                then
                  { model
                  | blinkStart =
                    model.elapsedTime
                  , heldItem =
                    Playing
                  , playerInventory =
                    let
                      inv = model.playerInventory
                    in
                      { inv
                      | heldItems =
                        model.playerInventory.heldItems ++
                          [ heldMino
                          ]
                      }
                  , currentScaleAnimation =
                    case
                      Set.toList <| toLocations <| heldMino
                    of
                      [] ->
                        Nothing
                      (a :: b) ->
                        Just
                          { fractionLeft = 1
                          , label = DropItem a
                          }
                  }
                else if
                  canAdd heldMino effectiveInventory
                then
                  let
                    tileInventoryShift =
                      Mino.width model.playerInventory.footprint + 1
                    -- Adds the held mino to an inventory
                    addToInventory : Inventory {} -> Inventory {}
                    addToInventory inventory =
                      { inventory
                      | heldItems =
                        inventory.heldItems ++
                          [ { heldMino
                            | llCorner =
                              tuplePlus
                                (-tileInventoryShift, 0)
                                heldMino.llCorner
                            }
                         ]
                      }

                    addToBin : Tile -> Tile
                    addToBin tile =
                      let
                        newTile =
                          case tile of
                            Bin binInv ->
                              addToInventory binInv |> Bin
                            _ ->
                              Wall
                      in
                        newTile
                  in
                    { model
                    | currentLevel =
                      let
                        currentLevel = model.currentLevel
                      in
                        Level.updateDoors <|
                          { currentLevel
                          | tiles =
                            currentLevel.tiles
                              |> map2dAt model.blockLocation addToBin
                          }
                    , blinkStart =
                      model.elapsedTime
                    , heldItem =
                      Playing
                    , currentScaleAnimation =
                      case
                        Set.toList <| toLocations <| heldMino
                      of
                        [] ->
                          Nothing
                        (a :: b) ->
                          Just
                            { fractionLeft = 1
                            , label = DropItem a
                            }
                    }
                else
                  model
              _ -> 
                let
                  (x, y) = heldMino.llCorner
                  (nextX, nextY) =
                    case move of
                      MoveUp    ->
                        (x, y-1)
                      MoveLeft  ->
                        (x-1, y)
                      MoveDown  ->
                        (x, y+1)
                      MoveRight ->
                        (x+1, y)
                      _         ->
                        (x, y)
                  nextLocation =
                    ( nextX
                      |> min
                        ( Mino.width effectiveInventory.footprint
                        - Mino.width heldMino.footprint
                        )
                        |> max 0
                    , nextY
                      |> min
                        ( Mino.height effectiveInventory.footprint
                        - Mino.height heldMino.footprint
                        )
                        |> max 0
                    )
                  newHoldingMino =
                    HoldingMino 
                      { heldMino
                      | llCorner =
                        nextLocation
                      }
                in
                  { model
                  | heldItem =
                    newHoldingMino
                  -- Animate the movement
                  , currentMoveAnimation =
                    Just
                      { fractionLeft = 1
                      , origin = (x, y)
                      , destinationHeldItem = newHoldingMino
                      , ghostItem = Nothing
                      }
                  }
          Selecting (x, y) ->
            case move of
              Select ->
                let
                  playerInventoryWidth = Mino.width model.playerInventory.footprint

                  tileInventory : Tile -> Maybe (Inventory {})
                  tileInventory tile =
                    case tile of
                      Bin binInv ->
                        Just binInv
                      _ ->
                        Nothing
                in
                  if
                    x < playerInventoryWidth
                  then
                    case
                      Inventory.pickUpAt (x, y) model.playerInventory
                    of
                      Nothing ->
                        model
                      Just (mino, newInventory) ->
                        { model
                        | playerInventory =
                          newInventory
                        -- Make the item held
                        , heldItem =
                          HoldingMino mino
                        , blinkStart =
                          model.elapsedTime - 750 * pi
                        -- Trigger the pick up animation
                        , currentScaleAnimation =
                          Just
                            { fractionLeft = 1
                            , label = PickUpItem
                            }
                        }
                  else
                    case
                      Level.tileAt
                        model.currentLevel
                        model.blockLocation
                        |> tileInventory
                          |> Maybe.andThen
                            ( Inventory.pickUpAt
                                ( x - playerInventoryWidth - 1
                                , y
                                )
                            )
                    of
                      Nothing ->
                        model
                      Just (pickedMino, newInventory) ->
                        { model
                        | currentLevel =
                          let
                            currentLevel = model.currentLevel
                          in
                            Level.updateDoors <|
                              { currentLevel
                              | tiles =
                                map2dAt
                                model.blockLocation
                                ( \tile ->
                                  case
                                    tile
                                  of
                                    Bin _ ->
                                      Bin newInventory
                                    _ ->
                                      -- Wall to indicate error
                                      Wall
                                )
                                model.currentLevel.tiles
                              }
                        , heldItem =
                          HoldingMino 
                            { pickedMino
                            | llCorner =
                              tuplePlus
                                ( playerInventoryWidth + 1
                                , 0
                                )
                                pickedMino.llCorner
                            }
                        , blinkStart =
                          model.elapsedTime - 750 * pi
                        -- Trigger the pick up animation
                        , currentScaleAnimation =
                          Just
                            { fractionLeft = 1
                            , label = PickUpItem
                            }
                        }
              _ ->
                let
                  (nextX, nextY) =
                    case move of
                      MoveUp    ->
                        (x, y-1)
                      MoveLeft  ->
                        (x-1, y)
                      MoveDown  ->
                        (x, y+1)
                      MoveRight ->
                        (x+1, y)
                      _         ->
                        (x, y)
                  nextLocation =
                    ( nextX
                      |> min
                        ( Mino.width effectiveInventory.footprint
                        - 1
                        )
                        |> max 0
                    , nextY
                      |> min
                        ( Mino.height effectiveInventory.footprint
                        - 1
                        )
                        |> max 0
                    )
                in
                  { model
                  | heldItem =
                    case move of
                      EnterSelection ->
                        Playing
                      _ ->
                        Selecting nextLocation
                  , currentMoveAnimation =
                    Just
                      { fractionLeft = 1
                      , origin = (x, y)
                      , destinationHeldItem = Selecting nextLocation
                      , ghostItem = Nothing
                      } 
                  }
            
          Playing ->
            let
              (x, y) = model.blockLocation
              nextLocation =
                case move of
                  MoveUp    -> (x, y-1)
                  MoveLeft  -> (x-1, y)
                  MoveDown  -> (x, y+1)
                  MoveRight -> (x+1, y)
                  _         -> (x, y)
              nextLocationTile =
                Level.tileAt model.currentLevel nextLocation
            in
              case move of
                EnterSelection ->
                  if
                    model.playerInventory.footprint == Set.fromList []
                  then
                    -- Don't open the inventory if it has no space
                    model
                  else
                    { model
                    | blinkStart =
                      model.elapsedTime
                    , blockLocation =
                      nextLocation
                    , heldItem =
                      Selecting (0, 0)
                    }
                _ ->
                  case
                    nextLocationTile
                  of
                    Pack tileInventory ->
                      if
                        nextLocation /= model.blockLocation
                      then
                        -- Swap the floor with the player
                        { model
                        | playerInventory =
                          tileInventory
                        , currentLevel =
                          Level.setTileAt
                            (Pack model.playerInventory)
                            nextLocation
                            model.currentLevel
                        , blockLocation =
                          nextLocation
                        , currentMoveAnimation =
                          Just
                            { fractionLeft = 1
                            , origin = (x, y)
                            , destinationHeldItem = Playing
                            , ghostItem = Nothing
                            }
                        }
                      else
                        { model
                        | blockLocation =
                          nextLocation
                        , currentMoveAnimation =
                          Just
                            { fractionLeft = 1
                            , origin = (x, y)
                            , destinationHeldItem = Playing
                            , ghostItem = Nothing
                            }
                        }
                    Item mino colorIndex ->
                      case
                        Inventory.findFits mino model.playerInventory |> Set.toList
                      of
                        -- If no fit is found
                        [ ] ->
                          { model
                          -- Animate the player trying to move
                          | currentScaleAnimation =
                            Just
                              { fractionLeft = 1
                              , label =
                                Impulse move mino
                              }
                          }
                        -- If exactly one fit is found
                        loc :: [] ->
                          { model
                          -- Move the player
                          | blockLocation =
                            nextLocation
                          -- Remove the item from the ground
                          , currentLevel =
                            Level.setTileAt Empty nextLocation model.currentLevel
                          -- Animate the player moving
                          , currentMoveAnimation =
                            Just
                              { fractionLeft = 1
                              , origin = (x, y)
                              , destinationHeldItem = Playing
                              -- Ghost item is rendered until the player arrives
                              , ghostItem = 
                                Just (nextLocationTile, nextLocation)
                              }
                          , currentScaleAnimation =
                            case
                              { footprint = mino
                              , llCorner = loc
                              }
                                |> toLocations
                                  |> Set.toList
                            of
                              [] ->
                                Nothing
                              (a :: b) ->
                                Just
                                  { fractionLeft = 1
                                  , label = DropItem a
                                  }
                          -- Put the item into the players inventory
                          , playerInventory =
                            let
                              playerInventory = model.playerInventory
                            in
                              { playerInventory
                              | heldItems = { colorIndex = colorIndex, footprint = mino, llCorner = loc } :: playerInventory.heldItems
                              }
                          }
                        -- If more than one fit is found
                        loc :: _ :: _ ->
                          { model
                          -- Move the player
                          | blockLocation =
                            nextLocation
                          -- Remove the item from the groud
                          , currentLevel =
                            Level.setTileAt Empty nextLocation model.currentLevel
                          -- Animate the player moving
                          , currentMoveAnimation =
                            Just
                              { fractionLeft = 1
                              , origin = (x, y)
                              , destinationHeldItem = Playing
                              -- Ghost item is rendered until the player arrives
                              , ghostItem = 
                                Just (nextLocationTile, nextLocation)
                              }
                          -- Put the item into the players hand
                          , heldItem =
                            HoldingMino
                              { colorIndex = colorIndex
                              , footprint = mino
                              , llCorner = loc
                              }
                          }
 
                    NewSlots pickedUpFootprint ->
                      let
                        playerInventory = model.playerInventory
                      in
                        if
                          Set.isEmpty playerInventory.footprint
                        then
                          { model
                          -- Make the slots the players inventory
                          | playerInventory =
                            { playerInventory 
                            | footprint =
                              pickedUpFootprint
                            }
                          -- Remove the new slots from the ground
                          , currentLevel =
                            Level.setTileAt 
                              Empty
                              nextLocation
                              model.currentLevel
                          -- Move the player
                          , blockLocation =
                            nextLocation
                          , currentMoveAnimation =
                            Just
                              { fractionLeft = 1
                              , origin = (x, y)
                              , destinationHeldItem = Playing
                              , ghostItem =
                                Just (nextLocationTile, nextLocation)
                              }
                          }
                        else
                          { model
                          -- Move the player
                          | blockLocation =
                            nextLocation
                          -- Pick Up the slots
                          , heldItem =
                            HoldingNewSlot
                              { llCorner =
                                (0, 0)
                              , footprint =
                                pickedUpFootprint
                              }
                          -- Remove the new slots from the ground
                          , currentLevel =
                            Level.setTileAt
                              Empty
                              nextLocation
                              model.currentLevel
                          , currentMoveAnimation =
                            Just
                              { fractionLeft = 1
                              , origin = (x, y)
                              , destinationHeldItem = Playing
                              , ghostItem =
                                Just (NewSlots pickedUpFootprint, nextLocation)
                              }
                          }

                    Bin mino ->
                      { model
                      | blockLocation =
                         nextLocation
                      , currentMoveAnimation =
                        Just
                          { fractionLeft = 1
                          , origin = (x, y)
                          , destinationHeldItem = Playing
                          , ghostItem = Nothing
                          }
                      }

                    Empty ->
                      { model
                      | blockLocation =
                         nextLocation
                      , currentMoveAnimation =
                        Just
                          { fractionLeft = 1
                          , origin = (x, y)
                          , destinationHeldItem = Playing
                          , ghostItem = Nothing
                          }
                      }

                    Win ->
                      case model.upcomingLevels of
                        [ ] ->
                          model
                        nextLevel :: remaining ->
                          initLevel
                            { model
                            | upcomingLevels = remaining
                            }
                            nextLevel

                    Wall ->
                      model

                    Door True ->
                      model

                    Door False ->
                      { model
                      | blockLocation =
                         nextLocation
                      , currentMoveAnimation =
                        Just
                          { fractionLeft = 1
                          , origin = (x, y)
                          , destinationHeldItem = Playing
                          , ghostItem = Nothing
                          }
                      }
    NoOp -> model
  , Cmd.none
  )
