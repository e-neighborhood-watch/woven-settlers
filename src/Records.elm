module Records exposing (..)

import Math.Vector2 exposing (Vec2)
import Math.Vector4 exposing (Vec4)

type alias Vertexed a =
  { a
  | coloration   : Vec4
  , abs_position : Vec2
  , rel_position : Vec2
  }

type alias Thicked a =
  { a
  | thickness : Float
  }

type alias Sized a =
  { a
  | size : Vec2
  }
