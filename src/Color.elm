module Color exposing (..) --(createUniqueColor)

import Math.Vector4 as Vec4 exposing (Vec4, vec4)

floatModBy : Int -> Float -> Float
floatModBy base toMod =
  let
    wholePart = floor toMod |> modBy base |> toFloat
    fracPart = toMod - (floor toMod |> toFloat)
  in wholePart + fracPart

-- From https://en.wikipedia.org/wiki/HSL_and_HSV#To_RGB
hslaToRgba : Vec4 -> Vec4
hslaToRgba hsla =
  let
    hue : Float
    hue =
      Vec4.getX hsla
    saturation : Float
    saturation =
      Vec4.getY hsla
    lightness : Float
    lightness =
      Vec4.getZ hsla
    alpha : Float
    alpha =
      Vec4.getW hsla
    chroma : Float
    chroma =
      (1 - abs (2*lightness - 1)) * saturation
    scaledHue : Float
    scaledHue =
      hue * 6
    minorChroma : Float
    minorChroma =
      chroma * (1 - abs (floatModBy 2 scaledHue - 1))
    lightnessShift : Float
    lightnessShift =
      lightness - chroma / 2
    lightnessShiftVector : Vec4
    lightnessShiftVector =
      vec4
        lightnessShift
        lightnessShift
        lightnessShift
        0
  in
    Vec4.add
      lightnessShiftVector
      ( if
         scaledHue <= 1
       then
         vec4
           chroma
           minorChroma
           0
           alpha
       else if
         scaledHue <= 2
       then
         vec4
           minorChroma
           chroma
           0
           alpha
       else if
         scaledHue <= 3
       then
         vec4
           0
           chroma
           minorChroma
           alpha
       else if
         scaledHue <= 4
       then
         vec4
           0
           minorChroma
           chroma
           alpha
       else if
         scaledHue <= 5
       then
         vec4
           minorChroma
           0
           chroma
           alpha
       else if
         scaledHue <= 6
       then
         vec4
           chroma
           0
           minorChroma
           alpha
       else
         vec4
           0
           0
           0
           alpha
      )

inversePhi : Float
inversePhi = (sqrt 5 - 1) / 2

createUniqueColor : Int -> Vec4
createUniqueColor id =
  vec4
    ( toFloat id*inversePhi |> floatModBy 1
    )
    1
    0.5
    1
    |> hslaToRgba

