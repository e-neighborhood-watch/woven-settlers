module Mino exposing
  ( Mino
  , width
  , height
  , boundingWidth
  , boundingHeight
  , tiles
  )

import Set exposing (Set)

-- All Minos are shifted to the lowest possible values such
-- that all positions remain nonnegative
type alias Mino
  = Set
    ( Int
    , Int
    )

boundingWidth : Mino -> Int
boundingWidth mino =
  if
    Set.isEmpty mino
  then
    0
  else
    ( Set.foldr (Tuple.first >> max) -1 >> (+) 1 ) mino

boundingHeight : Mino -> Int
boundingHeight mino =
  if
    Set.isEmpty mino
  then
    0
  else
    ( Set.foldr (Tuple.second >> max) -1 >> (+) 1) mino

width : Mino -> Int
width mino =
  let
    upBound = boundingWidth mino
  in
    if
      Set.isEmpty mino
    then
      0
    else
      upBound
        - Set.foldr (Tuple.first >> min) upBound mino

height : Mino -> Int
height mino =
 let
   upBound = boundingHeight mino
  in
    if
      Set.isEmpty mino
    then
      0
    else
     upBound
       - Set.foldr (Tuple.second >> min) upBound mino

tiles : Mino -> Int
tiles = Set.size
