module Levels.Flower exposing (levelData)

import Array
import Set exposing (Set)

import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 3
    , 1
    )
  , currentLevel =
    level
  }

tTet0 : Tile
tTet0 = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1), (1, 1)
    , (0, 0)
    ]
  )
  0  -- This number determines color

tTet1 : Tile
tTet1 = Item
  ( Set.fromList
    [ (0, 1), (1, 1), (2, 1)
    ,         (1, 0)
    ]
  )
  1  -- This number determines color

tTet2 : Tile
tTet2 = Item
  ( Set.fromList
    [         (1, 1)
    , (0, 0), (1, 0), (2, 0)
    ]
  )
  2  -- This number determines color

tTet3 : Tile
tTet3 = Item
  ( Set.fromList
    [         (1, 2)
    , (0, 1), (1, 1)
    ,         (1, 0)
    ]
  )
  3  -- This number determines color

-- An example hole
oHole : Tile
oHole = NewSlots
  ( Set.fromList
    [ (0, 1), (1, 1)
    , (0, 0), (1, 0)
    ]
  )

level : Level
level = 
  { tiles =
    -- Level Here
    [ [Wall,  Wall,   Wall, Wall,  Wall,  Wall,  Wall,  Wall]
    , [Wall, oHole, oHole,  Empty, Wall,  Wall, Wall,  Wall]
    , [Wall, oHole, oHole,  Empty, tTet3, tTet2, tTet1, tTet0]
    , [Wall,  Wall,   Wall, Wall,  Wall,  Wall,  Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }

