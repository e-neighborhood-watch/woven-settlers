module Levels.FlowerII
  exposing
    ( levelData
    )

import Array
import Set
  exposing
    ( Set
    )

import Level
  exposing
    ( Level
    , Tile (..)
    )
import LevelData
  exposing
    ( LevelData
    )

levelData : LevelData
levelData =
  { blockLocation =
    ( 3
    , 1
    )
  , currentLevel =
    level
  }

lTet0 : Tile
lTet0 = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1)
    , (0, 0), (1, 0)
    ]
  )
  0  -- This number determines color

lTet1 : Tile
lTet1 = Item
  ( Set.fromList
    [ (0, 1), (1, 1), (2, 1)
    , (0, 0)
    ]
  )
  1  -- This number determines color

lTet2 : Tile
lTet2 = Item
  ( Set.fromList
    [                 (2, 1)
    , (0, 0), (1, 0), (2, 0)
    ]
  )
  2  -- This number determines color

lTet3 : Tile
lTet3 = Item
  ( Set.fromList
    [ (0, 2), (1, 2)
    ,         (1, 1)
    ,         (1, 0)
    ]
  )
  3  -- This number determines color

-- An example hole
oHole : Tile
oHole = NewSlots
  ( Set.fromList
    [ (0, 1), (1, 1)
    , (0, 0), (1, 0)
    ]
  )

level : Level
level =
  { tiles =
    -- Level Here
    [ [Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
    , [Wall, oHole, oHole, Empty,  Wall,  Wall,  Wall,  Wall]
    , [Wall, oHole, oHole, Empty, lTet3, lTet2, lTet1, lTet0]
    , [Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }

