module Levels.CombineII exposing (levelData)

import Array
import Set exposing (Set)

import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 1
    , 1
    )
  , currentLevel =
    level
  }

zTet : Int -> Tile
zTet = Item
  ( Set.fromList
    [ (0, 1), (1, 1)
    ,         (1, 0), (2, 0)
    ]
  )


iHole : Tile
iHole = NewSlots
  ( Set.fromList
    [ (0, 0), (1, 0), (2, 0), (3, 0)
    ]
  )

level : Level
level = 
  { tiles =
    [ [Wall,  Wall,  Wall,   Wall,   Wall,  Wall]
    , [Wall, Empty, Empty,  iHole,  iHole,  Wall]
    , [Wall, Empty,  Wall,   Wall,   Wall,  Wall]
    , [Wall, Empty, Empty, zTet 0,  zTet 1, Empty]
    , [Wall,  Wall,  Wall,   Wall,   Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }

