module Levels.PickupII exposing (levelData)

import Array
import Set exposing (Set)

import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 2
    , 1
    )
  , currentLevel =
    level
  }

tTet0 : Tile
tTet0 = Item
  ( Set.fromList
    [         (1, 2)
    , (0, 1), (1, 1)
    ,         (1, 0)
    ]
  )
  0  -- This number determines color

tTet1 : Tile
tTet1 = Item
  ( Set.fromList
    [ (0, 1), (1, 1), (2, 1)
    ,         (1, 0)
    ]
  )
  1  -- This number determines color

sTet : Tile
sTet = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1), (1, 1)
    ,         (1, 0)
    ]
  )
  2  -- This number determines color

lTet : Tile
lTet = Item
  ( Set.fromList
    [ (0, 1)
    , (0, 0), (1, 0), (2, 0)
    ]
  )
  3  -- This number determines color

iTet : Tile
iTet = Item
  ( Set.fromList
    [ (0, 3)
    , (0, 2)
    , (0, 1)
    , (0, 0)
    ]
  )
  4  -- This number determines color

iHole : Tile
iHole = NewSlots
  ( Set.fromList
    [ (0, 0), (1, 0), (2, 0), (3, 0)
    ]
  )


zHole : Tile
zHole = NewSlots
  ( Set.fromList
    [ (0, 1), (1, 1)
    ,         (1, 0), (2, 0)
    ]
  )

level : Level
level =
  { tiles =
    -- Level Here
    [ [Wall,  Wall,   Wall, Wall,  Wall,  Wall,  Wall,  Wall]
    , [Wall, iHole,  Empty, Wall,  Wall, iHole,  Wall,  Wall]
    , [Wall, iHole,  Empty, lTet, tTet1, tTet0,  sTet,  iTet]
    , [Wall, iHole,  Empty, Wall,  Wall,  Wall, zHole,  Wall]
    , [Wall,  Wall,   Wall, Wall,  Wall,  Wall,  Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }

