module Levels.Win exposing (levelData)

import Array
import Set exposing (Set)

import Inventory exposing (Inventory)
import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 3
    , 3
    )
  , currentLevel =
    level
  }

dot : Tile
dot = Item
  ( Set.fromList
    [ (0, 0)
    ]
  )
  0

level : Level
level = 
  { tiles =
    [ [Wall,  Wall,   Wall,  Wall,   Wall,  Wall, Wall]
    , [Wall, Empty,  Empty, Empty,  Empty, Empty, Wall]
    , [Wall, Empty,    dot, Empty,    dot, Empty, Wall]
    , [Wall, Empty,  Empty, Empty,  Empty, Empty, Wall]
    , [Wall, Empty,    dot, Empty,    dot, Empty, Wall]
    , [Wall, Empty,  Empty, Empty,  Empty, Empty, Wall]
    , [Wall,  Wall,   Wall,  Wall,   Wall,  Wall, Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }

