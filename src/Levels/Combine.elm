module Levels.Combine exposing (levelData)

import Array
import Set exposing (Set)

import Inventory exposing (Inventory)
import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 1
    , 1
    )
  , currentLevel =
    level
  }

lTet : Tile
lTet = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1)
    , (0, 0), (1, 0)
    ]
  )
  1

jTet : Tile
jTet = Item
  ( Set.fromList
    [ (0, 2), (1, 2)
    ,         (1, 1)
    ,         (1, 0)
    ]
  )
  2

lHole : Tile
lHole = NewSlots
  ( Set.fromList
    [ (0, 1), (1, 1), (2, 1)
    , (0, 0)
    ]
  )

jHole : Tile
jHole = NewSlots
  ( Set.fromList
    [                 (2, 1)
    , (0, 0), (1, 0), (2, 0)
    ]
  )

level : Level
level = 
  { tiles =
    [ [Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
    , [Wall, Empty, Empty, lHole, jHole,  Wall]
    , [Wall, Empty,  Wall,  Wall,  Wall,  Wall]
    , [Wall, Empty, Empty,  lTet,  jTet, Empty]
    , [Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }
