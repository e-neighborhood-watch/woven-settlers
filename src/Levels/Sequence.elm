module Levels.Sequence exposing
  ( tailLevels
  , headLevel
  )

-- Makes sure the template compiles
import Levels.Template

-- Level Imports
import Levels.Overlap as OverlapLevel
import Levels.Test as TestLevel
import Levels.Move as MoveLevel
import Levels.Inventory as InventoryLevel
import Levels.Inventory2 as Inventory2Level
import Levels.Combine as CombineLevel
import Levels.Flower as FlowerLevel
import Levels.FlowerII as FlowerIILevel
import Levels.CombineII as CombineIILevel
import Levels.Fork as ForkLevel
import Levels.Pickup as PickupLevel
import Levels.PickupII as PickupIILevel
import Levels.Trash as TrashLevel
import Levels.Unlock as UnlockLevel
import Levels.Packed as PackedLevel
import Levels.Keys as KeyLevel
import Levels.Win as WinLevel
import Levels.Airlock as AirlockLevel

headLevel =
  MoveLevel.levelData

tailLevels =
  [ InventoryLevel.levelData
  , Inventory2Level.levelData
  , CombineLevel.levelData
  , FlowerLevel.levelData
  , FlowerIILevel.levelData
  , CombineIILevel.levelData
  , PackedLevel.levelData
  , OverlapLevel.levelData
  , ForkLevel.levelData
  , PickupLevel.levelData
  , PickupIILevel.levelData
  , TrashLevel.levelData
  , UnlockLevel.levelData
  , KeyLevel.levelData
  , AirlockLevel.levelData
  , WinLevel.levelData
  ]

