module Levels.Packed exposing (levelData)

import Array
import Set exposing (Set)

import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 4, 1 )
  , currentLevel =
    level
  }

-- Items --
-----------

oTet : Tile
oTet = Item
  ( Set.fromList
    [ (0, 1), (1, 1)
    , (0, 0), (1, 0)
    ]
  )
  1

tTet : Tile
tTet = Item
  ( Set.fromList
    [         (1, 2)
    , (0, 1), (1, 1)
    ,         (1, 0)
    ]
  )
  2 

sTet : Tile
sTet = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1), (1, 1)
    ,         (1, 0)
    ]
  )
  3 

-- Holes --
-----------

zHole1 : Tile
zHole1 = NewSlots
  ( Set.fromList
    [ (0, 1), (1, 1)
    ,         (1, 0), (2, 0)
    ]
  )

zHole2 : Tile
zHole2 = NewSlots
  ( Set.fromList
    [         (1, 2)
    , (0, 1), (1, 1)
    , (0, 0)
    ]
  )

tHole : Tile
tHole = NewSlots
  ( Set.fromList
    [ (0, 2)
    , (0, 1), (1, 1)
    , (0, 0)
    ]
  )

-- Level --
-----------

level : Level
level = 
  { tiles =
    List.reverse
      [ [Wall,   Wall,   Wall,   Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
      , [Wall,  tHole, zHole2, zHole1, Empty,  tTet,  oTet,  sTet, Empty]
      , [Wall,   Wall,   Wall,   Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
      ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = []
  }

