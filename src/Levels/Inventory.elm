module Levels.Inventory exposing (levelData)

import Array
import Set exposing (Set)

import Inventory exposing (Inventory)
import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 1
    , 1
    )
  , currentLevel =
    level
  }

tTet : Tile
tTet =
  Item
    ( Set.fromList
      [ (0, 2)
      , (0, 1), (1, 1)
      , (0, 0)
      ]
    )
    2

hole : Tile
hole =
  NewSlots
    ( Set.fromList
      [ (0, 2), (1, 2), (2, 2)
      , (0, 1), (1, 1), (2, 1)
      , (0, 0), (1, 0), (2, 0)
      ]
    )


level : Level
level = 
  { tiles =
    [ [Wall,  Wall,  Wall,  Wall,  Wall]
    , [Wall, Empty, Empty,  tTet, Empty]
    , [Wall, Empty,  Wall,  Wall,  Wall]
    , [Wall,  hole,  Wall,  Wall,  Wall]
    , [Wall,  Wall,  Wall,  Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }

