module Levels.Keys exposing (levelData)

import Array
import Set exposing (Set)

import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    -- Start location of the player relative to the upper left hand corner
    ( 5, 1 )
  , currentLevel =
    level
  }

-- Items --
-----------

lTri0 : Tile
lTri0 = Item
  ( Set.fromList
    [ (0, 1)
    , (0, 0), (1, 0)
    ]
  )
  1 

lTri1 : Tile
lTri1 = Item
  ( Set.fromList
    [ (0, 1), (1, 1)
    ,         (1, 0)
    ]
  )
  2 

iTri : Tile
iTri = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1)
    , (0, 0)
    ]
  )
  3

-- Holes --
-----------

iHole : Tile
iHole = NewSlots
  ( Set.fromList
    [ (0, 2)
    , (0, 1)
    , (0, 0)
    ]
  )

iHole2 : Tile
iHole2 = NewSlots
  ( Set.fromList
    [ (0, 0), (1, 0), (2, 0)
    ]
  )

tHole : Tile
tHole = NewSlots
  ( Set.fromList
    [ (0, 2)
    , (0, 1), (1, 1)
    , (0, 0)
    ]
  )

-- Bins --
----------

bin0 : Tile
bin0 = Bin
  { footprint =
    ( Set.fromList
      [ (0, 2), (1, 2), (2, 2)
      , (0, 1), (1, 1), (2, 1)
      , (0, 0), (1, 0), (2, 0)
      ]
    )
  , heldItems =
    [ { footprint =
        Set.fromList
          [         (1, 2)
          , (0, 1), (1, 1), (2, 1)
          ,         (1, 0)
          ]
      , colorIndex =
        4
      , llCorner =
        (0, 0)
      }
    ]
  }

bin1 : Tile
bin1 = Bin
  { footprint =
    ( Set.fromList
      [         (1, 2)
      , (0, 1), (1, 1), (2, 1)
      ,         (1, 0)
      ]
    )
  , heldItems =
    [ ]
  }

-- Doors --
-----------

-- Some aliases for doors

-- A closed door
doorC : Tile
doorC = Door True

-- An open door
doorO : Tile
doorO = Door False

-- Level --
-----------

level : Level
level = 
  { tiles =
    List.reverse
      [ [ Wall,   Wall,  Wall,  Wall, Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
      , [ Wall,  Empty, Empty, lTri1, Wall, Empty, Empty, Empty, Empty, Empty,  Wall]
      , [ Wall, iHole2,  Wall, tHole, iTri, iHole, Empty,  bin1,  bin0, Empty, doorC]
      , [ Wall,  Empty, Empty, lTri0, Wall, Empty, Empty, Empty, Empty, Empty,  Wall]
      , [ Wall,   Wall,  Wall,  Wall, Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
      ]
    |> List.map Array.fromList
      |> Array.fromList
  , links =
    [ { from = (8, 2)
      , to   = (10, 2)
      }
    ]
  }

