-- Change the module name
module Levels.Template exposing (levelData)

import Array
import Set exposing (Set)

import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    -- Start location of the player relative to the upper left hand corner
    ( 1, 1 )
  , currentLevel =
    level
  }

-- Items --
-----------

-- An example item
lTet : Tile
lTet = Item
  ( Set.fromList
    -- This is the shape of the item
    -- Add or remove coordinates to change the shape
    [ (0, 2)
    , (0, 1)
    , (0, 0), (1, 0)
    ]
  )
  1  -- This number determines color
     -- Items with the same number will have the same color
     -- give each item its own color

-- Another example item
oTet : Tile
oTet = Item
  ( Set.fromList
    -- This is the shape of the item
    -- Add or remove coordinates to change the shape
    -- order does not matter
    [ (0, 1), (1, 1)
    , (0, 0), (1, 0)
    ]
  )
  2  -- This number determines color
     -- Items with the same number will have the same color
     -- give each item its own color

-- Holes --
-----------

-- An example hole
lHole : Tile
lHole = NewSlots
  ( Set.fromList
    -- This is the shape of the hole
    -- Add or remove coordinates to change the shape
    -- order does not matter
    [ (0, 1), (1, 1), (2, 1)
    , (0, 0)
    ]
  )

-- Bins --
----------

-- An example bin
bin0 : Tile
bin0 = Bin
  { footprint =
    ( Set.fromList
      -- This is shape of the bin
      -- Add or remove coordinates to change the shape
      -- order does not matter
      [ (0, 2), (1, 2), (2, 2)
      , (0, 1), (1, 1), (2, 1)
      , (0, 0), (1, 0), (2, 0)
      ]
    )
  , heldItems =
    -- These are the items in the bin when the level starts
    -- This bin is empty
    [ ]
  }

-- A bin with something in it
bin1 : Tile
bin1 = Bin
  { footprint =
    ( Set.fromList
      -- This is shape of the bin
      -- Add or remove coordinates to change the shape
      -- order does not matter
      [ (0, 2), (1, 2), (2, 2)
      , (0, 1),         (2, 1)
      , (0, 0), (1, 0), (2, 0)
      ]
    )
  , heldItems =
    -- These are the items in the bin when the level starts
    -- This has an L-Tetramino in it
    -- Items here are formatted a lot like the items from above but with some extra info
    [ { footprint =
        Set.fromList
          -- This is the shape of the item in bin
          [ (0, 1)
          , (0, 0), (1, 0), (2, 0)
          ]
      , colorIndex =
        -- This number determines the color of the item
        -- It should be unique to the item
        3
      , llCorner =
        -- This is the location of the lower left cordinate of the item
        -- It is relative to the lower left corner of the bin
        (0, 0)
      }
    ]
  }

-- Doors --
-------------------

-- Some aliases for doors

-- A closed door
doorC : Tile
doorC = Door True

-- An open door
doorO : Tile
doorO = Door False

-- Level --
-----------

level : Level
level = 
  { tiles =
    List.reverse
    -- Level layout goes here
    -- You can use any of the objects defined above in addition to:
    -- Wall -> a wall
    -- Empty -> empty space
    -- Wall and Empty must be capitalized while objects referenced above must start with a lower case
    -- Edit below
      [ [Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
      , [Wall,  bin0,  bin1,  lTet,  oTet, lHole,  Wall]
      , [Wall, Empty, Empty, Empty, Empty, Empty, Empty]
      , [Wall,  bin0, Empty, Empty, Empty, doorO, Empty]
      , [Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
      ]
    -- Edit above
    |> List.map Array.fromList
      |> Array.fromList
  , links =
    -- These link a door with a bin
    -- When the bin is full the door will open
    [ { from = (1, 3) -- Coordinate of the bin
      , to   = (5, 3) -- Coordinate of the door it unlocks
      }
    ]
  }

