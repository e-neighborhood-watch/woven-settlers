module Levels.Overlap exposing (levelData)

import Array
import Set exposing (Set)

import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    -- Start location of the player relative to the lower left hand corner
    ( 1, 1 )
  , currentLevel =
    level
  }

-- Items --
-----------

-- An example item
lTet : Tile
lTet = Item
  ( Set.fromList
    [ (0, 2), (1, 2)
    ,         (1, 1)
    ,         (1, 0)
    ]
  )
  1 

tTet : Tile
tTet = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1), (1, 1)
    , (0, 0)
    ]
  )
  2

-- Holes --
-----------

jHole : Tile
jHole = NewSlots
  ( Set.fromList
    [ (0, 1), (1, 1), (2, 1)
    ,                 (2, 0)
    ]
  )

lHole : Tile
lHole = NewSlots
  ( Set.fromList
    [ (0, 1), (1, 1), (2, 1)
    , (0, 0)
    ]
  )

-- Level --
-----------

level : Level
level = 
  { tiles =
    List.reverse
      [ [Wall,  Wall,  Wall,  Wall,   Wall,  Wall]
      , [Wall, Empty, Empty, jHole,  lHole,  Wall]
      , [Wall, Empty, Empty,  Wall,   Wall,  Wall]
      , [Wall, Empty, Empty,  lTet,  jHole,  tTet]
      , [Wall,  Wall,  Wall,  Wall,   Wall,  Wall]
      ]
    |> List.map Array.fromList
      |> Array.fromList
  , links =
    [ ]
  }

