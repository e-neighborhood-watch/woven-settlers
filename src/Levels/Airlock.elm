module Levels.Airlock exposing
  ( levelData
  )

import Array
import Set exposing (Set)

import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    -- Start location of the player relative to the upper left hand corner
    ( 1, 1 )
  , currentLevel =
    level
  }

-- Items --
-----------

lTet : Tile
lTet = Item
  ( Set.fromList
    [                 (2, 1)
    , (0, 0), (1, 0), (2, 0)
    ]
  )
  1

i3_1 : Tile
i3_1 = Item
  ( Set.fromList
    [ (0, 0), (1, 0), (2, 0)
    ]
  )
  5

i3_2 : Tile
i3_2 = Item
  ( Set.fromList
    [ (0, 0), (1, 0), (2, 0)
    ]
  )
  8

pPen : Tile
pPen = Item
  ( Set.fromList
    [ (0, 1), (1, 1)
    , (0, 0), (1, 0), (2, 0)
    ]
  )
  7

-- Holes --
-----------

hole : Tile
hole = NewSlots
  ( Set.fromList
    [ (0, 1), (1, 1), (2, 1), (3, 1)
    , (0, 0), (1, 0), (2, 0), (3, 0)
    ]
  )

-- Bins --
----------

bin0 : Tile
bin0 = Bin
  { footprint =
    ( Set.fromList
      [                 (2, 3)
      , (0, 2), (1, 2), (2, 2)
      ,         (1, 1), (2, 1)
      ,         (1, 0), (2, 0)
      ]
    )
  , heldItems =
    [
    ]
  }

bin1 : Tile
bin1 = Bin
  { footprint =
    ( Set.fromList
      [ (0, 2), (1, 2)
      , (0, 1), (1, 1), (2, 1)
      , (0, 0), (1, 0), (2, 0)
      ]
    )
  , heldItems =
    [ { footprint =
        Set.fromList
          [ (0, 0), (1, 0)
          ]
      , colorIndex =
        4
      , llCorner =
        (0, 1)
      }
    ]
  }

bin2 : Tile
bin2 = Bin
  { footprint =
    ( Set.fromList
      [ (0, 2), (1, 2)
      , (0, 1), (1, 1), (2, 1)
      , (0, 0), (1, 0), (2, 0)
      ]
    )
  , heldItems =
    [ { footprint =
        Set.fromList
          [ (0, 0), (1, 0)
          ]
      , colorIndex =
        3
      , llCorner =
        (0, 0)
      }
    ]
  }

-- Doors --
-----------

-- Some aliases for doors

-- A closed door
doorC : Tile
doorC = Door True

-- An open door
doorO : Tile
doorO = Door False

-- Level --
-----------

level : Level
level =
  { tiles =
    List.reverse
      [ [ Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall, Wall]
      , [ Wall,  hole,  bin2, Empty, Empty,  Wall,  pPen,  i3_1,  i3_2, Wall]
      , [ Wall, Empty, Empty, Empty, Empty, doorC, Empty, Empty, Empty, Wall]
      , [ Wall, Empty, Empty,  lTet,  bin1,  Wall,  bin0, Empty, Empty,doorC]
      , [ Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall, Wall]
      ]
    |> List.map Array.fromList
      |> Array.fromList
  , links =
    [ { from =
        (6, 1)
      , to =
        (9, 1)
      }
    , { from =
        (2, 3)
      , to =
        (5, 2)
      }
    ]
  }

