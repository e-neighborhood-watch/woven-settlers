## Level design document

This document will teach you the basics of how to build your own Minno level.

If you come up with a cool level feel free to submit a pull request at https://gitlab.com/e-neighborhood-watch/woven-settlers

If you have any questions about this process you can leave an issue there too.

Levels are written in elm so experience in elm or something similar is pretty beneficial.
You can however, in theory, write your own levels without that experience.

## Set up 

To start your new level you should copy the template.
Navigate into the directory `src/Levels/` (where this file is located), and copy the template to a new file

    cp Template.elm NewLevel.elm

The next step is to update the module name.
The first two lines of your new file should be:

    -- Change the module name
    module Levels.Template exposing (levelData)

You should change the module name to match the name of the file (without the extension)

For example if we named our file `NewLevel.elm` like above we would change the second line to

    module Levels.NewLevel exposing (levelData)

You can then get rid of the first line (or leave it if you want it doesn't do anything).

## Level design

Now you can start to design your level.

The template you copied has a good number of comments explaining how everything is made.
For this part you should follow them.

## Playing your level

Whether you want to share it or just test it, you are going to need to play your level.
To do this open up the file `src/Levels/Sequence.elm`.

This file determines which levels show up in what order.

In order for it to read you need to add an import.
Below the line 

    -- Level Imports

add a new line that looks like

    import Levels.NewLevel as NickName

Where `NewLevel` is the name of the file you placed your level (without the extension) and `NickName` is just an arbitrary name used to refer to your file in the future.
The nick name must be capitalized
If you would like you can leave out the nick name doing just

    import Levels.NewLevel

In this case your module will be refered to as `Levels.` plus the name of your file (without the extension).

Now you can add your level to the sequence.

`headLevel` is the first level to be played, you should asjust this to your level when testing.
You can adjust this by changing

    MoveLevel.levelData

to 

    NickName.levelData

If your level is not the first level to be played (which is likely the case if you are no longer testing).
You can add it to `tailLevels`.
These are all the levels that get played after the first level in the order they appear.
Just follow the formatting here and insert your level where you think it is appropriate.

