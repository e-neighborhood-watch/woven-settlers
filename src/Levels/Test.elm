module Levels.Test exposing (levelData)

import Array
import Set exposing (Set)

import Inventory exposing (Inventory)
import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 1
    , 1
    )
  , currentLevel =
    level
  }

slotBlob = NewSlots
  <| Set.fromList
    [ (0,0)
    , (0,1)
    , (1,0)
    , (2,1)
    , (2,2)
    , (3,0)
    , (3,1)
    ]

lTetramino = Item
  <| Set.fromList
    [                 (2, 1)
    , (0, 0), (1, 0), (2, 0) ]
iTetramino = Item
  <| Set.fromList
    [ (0,0), (1, 0), (2, 0), (3, 0) ]
iTetraminoR = Item
  <| Set.fromList
    [ (0,0), (0, 1), (0, 2), (0, 3) ]
fourByFour = Bin
  { heldItems =
    [ ]
  , footprint =
    Set.fromList
      [ (0,0)
      , (0,1)
      , (0,2)
      , (0,3)
      , (1,0)
      , (1,1)
      , (1,2)
      , (1,3)
      , (2,0)
      , (2,1)
      , (2,2)
      , (2,3)
      , (3,0)
      , (3,1)
      , (3,2)
      , (3,3)
      ]
  }

simplePack : Tile
simplePack = Pack
  { heldItems =
    [ ]
  , footprint =
    Set.fromList
      [ (0,0)
      , (0,1)
      , (0,2)
      , (0,3), (1, 3), (2, 3), (3, 3), (4, 3)
      ]
  }

level : Level
level = 
  { tiles =
    (Array.fromList << List.map Array.fromList)
      [ [ Wall,      Wall,          Wall,         Wall,       Wall,         Wall ]
      , [ Wall,     Empty,         Empty,        Empty, simplePack,         Wall ]
      , [ Wall,     Empty,          Wall,         Wall,      Empty, lTetramino 1 ]
      , [ Wall,  slotBlob,          Wall,         Wall,      Empty,         Wall ]
      , [ Wall,     Empty,  iTetramino 2, lTetramino 4,      Empty,         Wall ]
      , [ Wall,      Wall,          Wall,         Wall,       Wall,         Wall ]
      ]
  , links =
    [ ]
  }
