module Levels.Fork exposing (levelData)

import Array
import Set exposing (Set)

import Inventory exposing (Inventory)
import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 2
    , 4
    )
  , currentLevel =
    level
  }

iTet0 : Tile
iTet0 = Item
  ( Set.fromList
    [ (0, 3)
    , (0, 2)
    , (0, 1)
    , (0, 0)
    ]
  )
  0

iTet1 : Tile
iTet1 = Item
  ( Set.fromList
    [ (0, 0), (1, 0), (2, 0), (3, 0)
    ]
  )
  1

tTet0 : Tile
tTet0 = Item
  ( Set.fromList
    [         (1, 1)
    , (0, 0), (1, 0), (2, 0)
    ]
  )
  2

tTet1 : Tile
tTet1 = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1), (1, 1)
    , (0, 0)
    ]
  )
  3

hole0 : Tile
hole0 = NewSlots
  ( Set.fromList
    [ (0, 3), (1, 3)
    , (0, 2), (1, 2)
    , (0, 1)
    , (0, 0)
    ]
  )

hole1 : Tile
hole1 = NewSlots
  ( Set.fromList
    [         (1, 2)
    , (0, 1), (1, 1)
    , (0, 0)
    ]
  )

hole2 : Tile
hole2 = NewSlots
  ( Set.fromList
    [ (0, 2), (1, 2)
    , (0, 1)
    , (0, 0)
    ]
  )

level : Level
level =
  { tiles =
    [ [Wall, iTet1,  Wall, tTet0,  Wall]
    , [Wall, hole1,  Wall, hole2,  Wall]
    , [Wall, iTet0,  Wall, tTet1,  Wall]
    , [Wall, Empty, hole0, Empty,  Wall]
    , [Wall, Empty, Empty, Empty,  Wall]
    , [Wall,  Wall,  Wall,  Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }

