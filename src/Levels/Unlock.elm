module Levels.Unlock exposing (levelData)

import Array
import Set exposing (Set)

import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 5
    , 2
    )
  , currentLevel =
    level
  }

oTet : Tile
oTet = Item
  ( Set.fromList
    [ (0, 1), (1, 1)
    , (0, 0), (1, 0)
    ]
  )
  1

oHole : Tile
oHole = NewSlots
  ( Set.fromList
    [ (0, 1), (1, 1)
    , (0, 0), (1, 0)
    ]
  )

oLock : Tile
oLock = Bin
  { footprint =
    ( Set.fromList
      [ (0, 1), (1, 1)
      , (0, 0), (1, 0)
      ]
    )
  , heldItems =
    [ ]
  }

fLock : Tile
fLock = Bin
  { footprint =
    ( Set.fromList
      [ (0, 1), (1, 1)
      , (0, 0), (1, 0)
      ]
    )
  , heldItems =
    [ { footprint =
        ( Set.fromList
          [ (0, 1), (1, 1)
          , (0, 0), (1, 0)
          ]
        )
      , llCorner =
        (0, 0)
      , colorIndex =
        0
      }
    ]
  }

oBin : Tile
oBin = Bin
  { footprint =
    ( Set.fromList
      [ (0, 1), (1, 1)
      , (0, 0), (1, 0)
      ]
    )
  , heldItems =
    [ ]
  }

doorc : Tile
doorc = Door True

dooro : Tile
dooro = Door False

level : Level
level = 
  { tiles =
    [ [  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
    , [  Wall, Empty, Empty,  Wall, oLock, oHole, fLock,  Wall, Empty, Empty,  Wall]
    , [  Wall,  oTet, Empty, doorc, Empty, Empty, Empty, dooro, Empty, Empty,  Wall]
    , [  Wall, Empty, Empty,  Wall, Empty, Empty, Empty,  Wall, Empty, oLock,  Wall]
    , [  Wall,  Wall, doorc,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links =
    [ { to = (3, 2)
      , from = (4, 1)
      }
    , { to = (7, 2)
      , from = (6, 1)
      }
    , { to = (2, 4)
      , from = (9 ,3)
      }
    ]
  }

