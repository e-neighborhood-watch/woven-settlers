module Levels.Trash exposing (levelData)

import Array
import Set exposing (Set)

import Inventory exposing (Inventory)
import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 3
    , 2
    )
  , currentLevel =
    level
  }

lTet : Tile
lTet = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1)
    , (0, 0), (1, 0)
    ]
  )
  0
   
oTet : Tile
oTet = Item
  ( Set.fromList
    [ (0, 1), (1, 1)
    , (0, 0), (1, 0)
    ]
  )
  2

block : Tile
block = Item
  ( Set.fromList
    [ (0, 2), (1, 2), (2, 2)
    , (0, 1), (1, 1), (2, 1)
    , (0, 0), (1, 0), (2, 0)
    ]
  )
  3

sTet : Tile
sTet = Item
  ( Set.fromList
    [ (0, 2)
    , (0, 1), (1, 1)
    ,         (1, 0)
    ]
  )
  4

jTet : Tile
jTet = Item
  ( Set.fromList
    [ (0, 2), (1, 2)
    ,         (1, 1)
    ,         (1, 0)
    ]
  )
  5

hole0 : Tile
hole0 = NewSlots
  ( Set.fromList
    [ (0, 2)
    , (0, 1), (1, 1)
    , (0, 0), (1, 0)
    ]
  )
  
hole1 : Tile
hole1 = NewSlots
  ( Set.fromList
    [ (0, 2), (1, 2)
    ,         (1, 1)
    ,         (1, 0)
    ]
  )

hole2 : Tile
hole2 = NewSlots
  ( Set.fromList
    [ (0, 1)
    , (0, 0)
    ]
  )

hole3 : Tile
hole3 = NewSlots
  ( Set.fromList
    [ (0, 0), (1, 0)
    ]
  )

bin0 : Tile
bin0 = Bin
  { footprint =
    ( Set.fromList
      [         (1, 3)
      , (0, 2), (1, 2), (2, 2)
      , (0, 1), (1, 1), (2, 1)
      , (0, 0), (1, 0), (2, 0)
      ]
    )
  , heldItems =
    [ ]
  }

bin1 : Tile
bin1 = Bin
  { footprint =
    ( Set.fromList
      [ (0, 2), (1, 2), (2, 2)
      , (0, 1),         (2, 1)
      , (0, 0), (1, 0), (2, 0)
      ]
    )
  , heldItems =
    [ ]
  }
  

level : Level
level = 
  { tiles =
    [ [Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
    , [Wall, hole0, Empty, Empty,  lTet, Empty, Empty, Empty, Empty, Empty,  Wall]
    , [Wall,  Wall, Empty, Empty,  Wall,  oTet,  Wall,  sTet,  Wall,  jTet,  Wall]
    , [Wall,  bin0, Empty, Empty,  Wall, hole2,  Wall, hole3,  Wall,  bin1,  Wall]
    , [Wall,  Wall,  Wall, block,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }

