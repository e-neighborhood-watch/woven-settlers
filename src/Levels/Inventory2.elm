module Levels.Inventory2 exposing (levelData)

import Array
import Set exposing (Set)

import Inventory exposing (Inventory)
import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 3
    , 2
    )
  , currentLevel =
    level
  }

iTet : Tile
iTet = Item
  ( Set.fromList
    [ (0, 0)
    , (0, 1)
    , (0, 2)
    , (0, 3)
    ]
  )
  0

barTet : Tile
barTet = Item
  ( Set.fromList
    [ (0, 0), (1, 0), (2, 0), (3, 0)
    ]
  )
  1

hole : Tile
hole =
  NewSlots
    ( Set.fromList
      [ (0, 4)
      , (0, 3)
      , (0, 2)
      , (0, 1)
      , (0, 0), (1, 0), (2, 0), (3, 0)
      ]
    )

level : Level
level = 
  { tiles =
    [ [Wall,  Wall,  Wall, Wall, Wall,   Wall]
    , [Wall,  hole,  Wall, Empty, iTet, barTet]
    , [Wall, Empty, Empty, Empty, Wall,   Wall]
    , [Wall,  Wall,  Wall,  Wall, Wall,   Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }

