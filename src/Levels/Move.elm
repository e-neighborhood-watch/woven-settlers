module Levels.Move exposing (levelData)

import Array
import Set exposing (Set)

import Inventory exposing (Inventory)
import Level exposing (Level, Tile (..))
import LevelData exposing (LevelData)

levelData : LevelData
levelData =
  { blockLocation =
    ( 1
    , 1
    )
  , currentLevel =
    level
  }

level : Level
level =
  { tiles =
    [ [Wall,  Wall,  Wall,  Wall]
    , [Wall, Empty, Empty,  Wall]
    , [Wall, Empty, Empty, Empty]
    , [Wall,  Wall,  Wall,  Wall]
    ]
    |> List.map Array.fromList
      |> Array.fromList
  , links = [ ]
  }
