module Shader exposing
  ( vertexShader
  , squareOutlineShader
  , circleOutlineShader
  , squareFillShader
  , circleFillShader
  )

import Records exposing (..)

import WebGL exposing (Shader)

import Math.Vector2 as Vec2 exposing (Vec2)
import Math.Vector4 as Vec4 exposing (Vec4)

vertexShader : Shader (Vertexed v) (Sized a) { pos : Vec2, color : Vec4 }
vertexShader =
  [glsl|
    attribute vec4 coloration;
    attribute vec2 abs_position;
    attribute vec2 rel_position;
    varying vec2 pos;
    varying vec4 color;
    uniform vec2 size;

    vec2 rescale(vec2 pos, vec2 maxSize) {
      return 2.0*pos/maxSize - 1.0;
    }

    void main () {
      gl_Position = vec4(rescale(abs_position, size), 0.0, 1.0);
      pos = rel_position;
      color = coloration;
    }

  |]

squareOutlineShader : Shader {} (Thicked (Sized a)) { pos : Vec2, color : Vec4 }
squareOutlineShader = 
  [glsl|
    precision mediump float;
    varying vec2 pos;
    uniform float thickness;
    varying vec4 color;
    const float power = 6.0;
    const float sharpness = 1.5;

    void main () {
      vec2 uv = (pos * 2.0 - 1.0) * 1.1;
      float opacity = sharpness * (1.0 - abs(thickness * (1.0 - (pow(abs(uv.x), power) + pow(abs(uv.y), power)))));
      gl_FragColor = vec4(color.xyz, opacity*color.w);
    }

  |]

circleOutlineShader : Shader {} (Thicked (Sized a)) { pos : Vec2, color : Vec4 }
circleOutlineShader = 
  [glsl|
    precision mediump float;
    varying vec2 pos;
    uniform float thickness;
    varying vec4 color;
    const float power = 2.0;
    const float sharpness = 5.0;

    void main () {
      vec2 uv = (pos * 2.0 - 1.0) * 1.2;
      float opacity = sharpness * (1.0 - abs(2.0 * thickness * (1.0 - (pow(abs(uv.x), power) + pow(abs(uv.y), power)))));
      gl_FragColor = vec4(color.xyz, opacity*color.w);
    }

  |]

squareFillShader : Shader {} (Sized a) { pos : Vec2, color : Vec4 }
squareFillShader =
  [glsl|
    precision mediump float;
    varying vec2 pos;
    varying vec4 color;
    const float power = 6.0;
    const float sharpness = 8.0;

    void main () {
      vec2 uv = (pos*2.0 - 1.0)*1.1;
      float opacity = sharpness * (1.2 - (pow(abs(uv.x), power) + pow(abs(uv.y), power)));
      gl_FragColor = vec4(color.xyz, opacity*color.w);
    }

  |]

circleFillShader : Shader {} (Sized a) { pos : Vec2, color : Vec4 }
circleFillShader =
  [glsl|
    precision mediump float;
    varying vec2 pos;
    varying vec4 color;
    const float power = 2.0;
    const float sharpness = 8.0;

    void main () {
      vec2 uv = (pos*2.0 - 1.0)*1.1;
      float opacity = sharpness * (1.2 - (pow(abs(uv.x), power) + pow(abs(uv.y), power)));
      gl_FragColor = vec4(color.xyz, opacity*color.w);
    }

  |]
