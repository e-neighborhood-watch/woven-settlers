module Inventory exposing
  ( Inventory
  , Region
  , Colored
  , toLocations
  , canAdd
  , join
  , pickUpAt
  , mergeSlots
  , findFits
  , empty
  , occupiedSpaces
  )

import Mino exposing
  ( Mino
  )
import Basics2 exposing
  ( tuplePlus
  , tupleMinus
  , overlaps
  , isWithin
  , maybeOr
  , maybeGuard
  , flip
  )

import Basics exposing
  ( not
  )
import Set exposing (Set)

type alias Region a =
  { a
  | footprint :
    Mino
  , llCorner :
    ( Int
    , Int
    )
  }

type alias Colored a =
  { a
  | colorIndex :
    Int
  }

type alias Inventory a =
  { heldItems :
    List (Colored (Region a))
  , footprint :
    Mino
  }

empty : Inventory {}
empty =
  { heldItems =
    [ ]
  , footprint =
    Set.empty
  }

mergeSlots :
  (
   Region a
  -> 
   Inventory b
  -> 
   Inventory b
  )
mergeSlots newSlots inventory =
  let
    (x, y) = newSlots.llCorner
    offset =
      ( max (-x) 0
      , max (-y) 0
      )
  in
    { inventory
    | footprint =
      toLocations newSlots
        |> Set.union inventory.footprint
        |> Set.map (tuplePlus offset)
    , heldItems =
      List.map
        (\item ->
          { item
          | llCorner =
            tuplePlus offset item.llCorner
          }
        )
        inventory.heldItems
    }

pickUpAt : 
  (
    (Int, Int)
  ->
    Inventory a
  ->
    Maybe
      ( Colored (Region a)
      , Inventory a
      )
  )
pickUpAt loc inventory =
  let
    go :
      (
        List (Colored (Region a))
      ->
        Maybe (Colored (Region a), List (Colored (Region a)))
      )
    go list =
      case list of
        [] ->
          Nothing
        (a :: b) ->
          if
            toLocations a
              |> Set.member loc 
          then
            Just
              ( a
              , b
              )
           else
             case
               go b
             of
               Nothing ->
                 Nothing
               Just
                 ( mino
                 , newList
                 ) ->
                   Just
                     ( mino
                     , a :: newList
                     )
  in
    case
      go inventory.heldItems
    of
      Nothing ->
        Nothing
      Just (pickedMino, newHeldItems) ->
        Just
          ( pickedMino
          , { inventory
            | heldItems =
              newHeldItems
            }
          )
 
toLocations :
  (
    Region a
  ->
    Set
      ( Int
      , Int
      )
  )
toLocations
  { llCorner 
  , footprint
  }
  = Set.map
    ( tuplePlus llCorner
    ) footprint
    
occupiedSpaces :
  Inventory a
    -> Set
      ( Int
      , Int
      )
occupiedSpaces inventory
  = List.foldl (toLocations >> (Set.union)) Set.empty inventory.heldItems

join :
  (
    Inventory a
  ->
    Inventory a
  ->
    Inventory a
  )
join inv1 inv2 =
  let
    shift = tuplePlus (Mino.width inv1.footprint + 1, 0)
  in
    if
      Set.isEmpty inv1.footprint
    then
      inv2
    else
      { heldItems =
        inv1.heldItems ++
          List.map
            (\x -> { x | llCorner = shift x.llCorner })
            inv2.heldItems
      , footprint =
        Set.union inv1.footprint <| Set.map shift inv2.footprint
      }

findFits : Mino -> Inventory {} -> Set (Int, Int)
findFits mino inventory =
  let
    buildRegion : (Int, Int) -> Region {}
    buildRegion llCorner =
      { footprint = mino
      , llCorner = llCorner
      }
    minoTestSquare : Maybe (Int,Int)
    minoTestSquare =
      mino
        |> Set.toList
        |> List.head
  in
    case minoTestSquare of
      -- If our mino is empty (?!)
      Nothing ->
        Set.singleton (0, 0)
      Just minoSquare ->
        occupiedSpaces inventory
          |> Set.diff inventory.footprint
            |> Set.map (flip tupleMinus minoSquare)
              |> Set.filter (buildRegion >> flip canAdd inventory)

canAdd :
  (
    Region a
  ->
    Inventory b
  ->
    Bool
  )
canAdd
  newMino
  inventory
  =
    let
      minoLocations = toLocations newMino
    in
      occupiedSpaces inventory
        |> overlaps minoLocations
          |> not
            |> (&&) (isWithin minoLocations inventory.footprint) 
