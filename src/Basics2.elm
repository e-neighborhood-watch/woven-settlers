module Basics2 exposing (..)

import Set exposing (Set)

import Basics

import Array exposing (Array)

import Math.Vector2 as Vec2 exposing (vec2, Vec2)

signum : number -> number
signum num =
  if
    num > 0
  then
    1
  else if
    num < 0
  then
    -1
  else
    num

maybeOr : Maybe a -> Maybe a -> Maybe a
maybeOr maybe1 maybe2 =
  case maybe1 of
    Nothing -> maybe2
    Just x -> maybe1

maybeGuard : Bool -> a -> Maybe a
maybeGuard bool =
  if
    bool
  then
    Just
  else
    const Nothing

mapAt : Int -> (a -> a) -> Array a -> Array a
mapAt index f ar =
  case Array.get index ar of
    Nothing ->
      ar
    Just x ->
      Array.set index (f x) ar

map2dAt : (Int, Int) -> (a -> a) -> Array (Array a) -> Array (Array a)
map2dAt (x, y) f = mapAt x f |> mapAt y

flip : (a -> b -> c) -> (b -> a -> c)
flip f a b = f b a

const : a -> b -> a
const a b = a

-- Is aparently in Basics acording to the docs
-- but the compiler disagrees
uncurry : (a -> b -> c) -> (a, b) -> c
uncurry f (a, b) = f a b

tuplePlus :
  (
    ( Int
    , Int
    )
  ->
    ( Int
    , Int
    )
  ->
    ( Int
    , Int
    )
  )
tuplePlus
  ( a
  , b
  )
  ( c
  , d
  )
  = ( a + c
    , b + d
    )

tupleMinus :
  (
    ( Int
    , Int
    )
  ->
    ( Int
    , Int
    )
  ->
    ( Int
    , Int
    )
  )
tupleMinus
  ( a
  , b
  )
  ( c
  , d
  )
  = ( a - c
    , b - d
    )

isWithin :
  (
    Set comparable
  ->
    Set comparable
  ->
    Bool 
  )
isWithin inner outer =
  Set.diff inner outer
    |> Set.isEmpty

overlaps :
  (
    Set comparable
  ->
    Set comparable
  ->
    Bool
  )
overlaps unsortedXs unsortedYs =
  Set.intersect unsortedXs unsortedYs
    |> Set.isEmpty
      |> not

makeVec2 : (Int, Int) -> Vec2
makeVec2 (a, b) =
  vec2 (toFloat a) (toFloat b)
