module View exposing (view)

-- Internal Imports
import Animation exposing
  ( AnimationLabel (..)
  )
import Basics2 exposing (..)
import Holding exposing
  ( Holding (..)
  )
import Inventory exposing
  ( toLocations
  )
import Level exposing
  ( Tile (..)
  , Link
  )
import Mino
import Model exposing 
  ( PrgmModel
  )
import Message exposing
  ( PrgmMsg
  )
import Mesh exposing (..)
import Shader exposing (..)

-- External Imports
import Maybe
import Set exposing (Set)
import Html exposing
  ( Html
  )
import Html.Attributes exposing
  ( width
  , height
  , style
  )
import Math.Vector2 exposing
  ( vec2
  )
import Math.Vector4 exposing
  ( vec4
  )
import WebGL exposing
  (Mesh
  , Shader
  , alpha
  , antialias
  )
import WebGL.Settings.Blend as Blend exposing
  ( srcAlpha
  , oneMinusSrcAlpha
  )

view : PrgmModel -> Html PrgmMsg
view model =
  let
    levelSizeRecord =
      { size =
        vec2
          ( Level.width model.currentLevel
            |> toFloat
          ) 
          ( Level.height model.currentLevel
            |> toFloat
          )
      }
    wallRecord =
      { size =
        levelSizeRecord.size
      , thickness =
        3.0
      }
    binRecord =
      { size =
        levelSizeRecord.size
      , thickness =
        1.0
      }
    uncompletedLinkRecord =
      { size =
        levelSizeRecord.size
      , thickness =
        1.0
      }
    effectiveInventory =
      case
        Level.tileAt model.currentLevel model.blockLocation
      of
        Bin inv2 ->
          Inventory.join model.playerInventory inv2
        _ ->
          model.playerInventory
    effectiveLevel =
      case
        model.currentMoveAnimation |> Maybe.andThen .ghostItem
      of
        Nothing ->
          model.currentLevel
        Just (tile, pos) ->
          let
            currentLevel = model.currentLevel
          in
            { currentLevel
            | tiles =
              map2dAt pos (const tile) currentLevel.tiles
            }
    otherSlots =
      case model.heldItem of
        HoldingNewSlot x ->
          toLocations x
        _ ->
          Set.empty
    inventoryOffset =
      case model.heldItem of
        HoldingNewSlot heldNewSlot ->
          let
            (x, y) = heldNewSlot.llCorner
          in
            ( max (-x) 0
            , max (-y) 0
            )
        _ ->
          (0, 0)

    -- The dimensions of a ghost item
    -- Displayed when an item cannot fit
    (ghostWidth, ghostHeight) =
      case
        Maybe.map .label model.currentScaleAnimation
      of
        Just (Impulse _ ghostItem) ->
          ( Mino.width ghostItem
          , Mino.height ghostItem
          )
        _ ->
          -- Zero if there is no animation
          (0, 0)

    -- The dimensions of the entire rendering
    -- In number of tiles
    renderWidth =
       Tuple.first inventoryOffset +
         Mino.width effectiveInventory.footprint
           |> max ( Mino.boundingWidth otherSlots )
             -- At least the height of the ghost item
             |> max ghostWidth
    renderHeight =
       Tuple.second inventoryOffset +
         Mino.height effectiveInventory.footprint
           |> max ( Mino.boundingHeight otherSlots )
             -- At least the width of the ghost item
             |> max ghostHeight
          
    inventorySizeRecord =
      { size =
        vec2
          ( renderWidth
            |> toFloat
          ) 
          ( renderHeight
            |> toFloat
          )
      }
    inventorySpaceRecord =
      { size =
        inventorySizeRecord.size
      , thickness =
        3.0
      }
    newSlotsRecord =
      { size =
        inventorySizeRecord.size
      , thickness =
        0.5
      }
    cursorRecord =
      { size =
        inventorySizeRecord.size
      , thickness =
        0.125
      }
    linksOccupiedness =
      let
        linkOccupiedness : Link -> (Link, Float)
        linkOccupiedness link =
          ( link
          , case
              Level.tileAt model.currentLevel link.from
            of
              Bin inv ->
                (Inventory.occupiedSpaces inv |> Set.size |> toFloat)
                  / (inv.footprint |> Set.size |> toFloat)
              _ -> 0.0
          )
      in
        List.map linkOccupiedness model.currentLevel.links
  in 
    Html.div []
      [ WebGL.toHtmlWith
        [ alpha False
        , antialias
        ]
        [ 60 * Level.width model.currentLevel
          |> width
        , 60 * Level.height model.currentLevel
          |> height
        , style "display" "block"
        ]
        -- Walls
        [ WebGL.entity
          vertexShader
          squareFillShader
          (Mesh.buildWallsMesh model.currentLevel)
          wallRecord
        -- Open Doors
        , WebGL.entity
          vertexShader
          squareOutlineShader
          (Mesh.buildOpenDoorsMesh model.currentLevel)
          wallRecord
        -- Completed Links
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          circleFillShader
          (Mesh.buildCompletedLinksMesh linksOccupiedness)
          levelSizeRecord
        -- Uncompleted Links
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          circleOutlineShader
          (Mesh.buildUncompletedLinksMesh linksOccupiedness)
          uncompletedLinkRecord
        -- Items
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareFillShader
          (Mesh.buildItemsMesh effectiveLevel)
          levelSizeRecord
        -- Items
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareFillShader
          (Mesh.buildItemsMesh effectiveLevel)
          levelSizeRecord
        -- New Slots
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareOutlineShader
          (Mesh.buildNewSlotsMesh effectiveLevel)
          binRecord
        -- Bin outlines
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareOutlineShader
          ( Mesh.buildBinsMesh
            (vec4 0.8  0.8  0.8  1.0)
            (vec4 0.75 0.75 0.25 1.0)
            (vec4 0.25 0.25 0.75 1.0)
            model
          )
          binRecord
        -- Bin outlines
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareFillShader
          ( Mesh.buildBinItemsMesh
            (vec4 0.8  0.8  0.8  1.0)
            (vec4 0.75 0.75 0.25 1.0)
            (vec4 0.25 0.25 0.75 1.0)
            model
          )
          binRecord
        -- Player
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareFillShader
          ( Mesh.buildBlockMesh
            ( case model.heldItem of
              Playing -> True
              _ -> False
            )
            model
          )
          levelSizeRecord
        ]
      , Html.hr
        []
        []
      , WebGL.toHtmlWith
        [ alpha False
        , antialias
        ]
        [ 50 * renderWidth
          |> width
        , 50 * renderHeight
          |> height
        , style "display" "block"
        ]
        -- Inventory Outline
        [ WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareOutlineShader
          ( Mesh.buildInventoryOutlineMesh
            (vec4 0.8 0.8 0.8 1.0)
            inventoryOffset
            effectiveInventory
          )
          inventorySpaceRecord
        -- Inventory Items
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareFillShader
          ( Mesh.buildInventoryPiecesMesh
            model
            inventoryOffset
            effectiveInventory
          )
          inventorySizeRecord
        -- Animated ghost item
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareFillShader
          ( Mesh.buildInventoryGhostMesh
            model.currentScaleAnimation
          )
          inventorySizeRecord
        -- Held New Slots
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareOutlineShader
          ( Mesh.buildHeldNewSlotMesh inventoryOffset model.heldItem)
          newSlotsRecord
        -- Held Item
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareFillShader
          ( Mesh.buildHeldItemMesh
            model.heldItem
            model.currentMoveAnimation
            model.currentScaleAnimation
          )
          inventorySizeRecord
        -- Selector
        , WebGL.entityWith
          [ Blend.add srcAlpha oneMinusSrcAlpha ]
          vertexShader
          squareFillShader
          (Mesh.buildCursorMesh model.heldItem model.currentMoveAnimation)
          inventorySpaceRecord
        ]
      ]

