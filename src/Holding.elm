module Holding exposing
  ( Holding
    ( ..
    )
  )

import Mino exposing (Mino)
import Inventory exposing
  ( Region
  , Colored
  )

type Holding
  = Playing
  | HoldingMino (Colored (Region {}))
  | HoldingNewSlot (Region {})
  | Selecting (Int, Int)

