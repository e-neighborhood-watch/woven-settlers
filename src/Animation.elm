module Animation exposing
  ( Animated
  , Moving
  , Labeled
  , AnimationLabel (..)
  , advanceAnimation
  , moveAnimationOffset
  )

import Holding exposing (Holding)
import Movement exposing (Movement)
import Mino exposing (Mino)
import Level exposing
  ( Tile
  )

import Math.Vector2 as Vec2 exposing (Vec2, vec2)

type AnimationLabel
  = Impulse Movement Mino
  | DropItem (Int, Int)
  | PickUpItem

type alias Labeled a =
  { a
  | label : AnimationLabel
  }

type alias Animated a =
  { a
  | fractionLeft : Float
  }

type alias Moving a =
  { a
  | origin : (Int, Int)
  , destinationHeldItem : Holding
  , ghostItem : Maybe (Tile, (Int, Int))
  }

totalAnimationLength : Float
totalAnimationLength = 100

advanceAnimation : Float -> Animated a -> Maybe (Animated a)
advanceAnimation elapsed animation =
  if
    animation.fractionLeft <= elapsed / totalAnimationLength
  then
    Nothing
  else
    Just
      { animation
      | fractionLeft =
        animation.fractionLeft - elapsed / totalAnimationLength
      }

moveAnimationOffset : (Int, Int) -> Moving (Animated a) -> Vec2
moveAnimationOffset (destX, destY) { fractionLeft, origin } =
  let
    (origX, origY) = origin
  in
    vec2
      ( origX - destX |> toFloat )
      ( origY - destY |> toFloat )
      |> Vec2.scale fractionLeft
