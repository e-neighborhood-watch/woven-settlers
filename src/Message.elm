module Message exposing
  ( PrgmMsg (..)
  )

import Movement exposing (Movement)

type PrgmMsg
  = TimeHappens Float
  | UserMovement Movement
  | NoOp

