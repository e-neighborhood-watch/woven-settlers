module Movement exposing
  ( Movement (..)
  , toMovement
  )

type Movement
  = NoMovement
  | MoveUp
  | MoveLeft
  | MoveDown
  | MoveRight
  | Select
  | EnterSelection
  | RestartLevel

toMovement : String -> Movement
toMovement string =
  case string of
    "ArrowDown" ->
      MoveUp
    "s" ->
      MoveUp
    "z" ->
      MoveUp
    "ArrowLeft" ->
      MoveLeft
    "a" ->
      MoveLeft
    "q" ->
      MoveLeft
    "ArrowUp" ->
      MoveDown
    "w" ->
      MoveDown
    "ArrowRight" ->
      MoveRight
    "d" ->
      MoveRight
    "ESC" ->
      EnterSelection
    "Escape" ->
      EnterSelection
    "i" ->
      EnterSelection
    "e" ->
      EnterSelection
    " " ->
      Select
    "Enter" ->
      Select
    "r" ->
      RestartLevel
    _   ->
      NoMovement

