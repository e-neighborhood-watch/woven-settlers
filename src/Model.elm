module Model exposing
  ( PrgmModel
  , VolatileData
  , CoreData
  )

-- Internal Imports
import LevelData exposing
  ( LevelData
  )
import Holding exposing
  ( Holding
  )
import Animation exposing
  ( Animated
  , Moving
  , Labeled
  )
import Inventory exposing
  ( Inventory
  )

type alias CoreData a =
  { a
  | elapsedTime : Float
  , blinkStart : Float
  , upcomingLevels : List LevelData
  }

type alias VolatileData a =
  { a
  | heldItem : Holding
  , startOfLevel : LevelData
  , currentMoveAnimation : Maybe (Moving (Animated {}))
  , currentScaleAnimation : Maybe (Labeled (Animated {}))
  , playerInventory : Inventory {}
  }

type alias PrgmModel = CoreData (VolatileData LevelData)

