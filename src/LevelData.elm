module LevelData exposing
  ( LevelData
  )

import Inventory exposing (Inventory)
import Level exposing (Level)

type alias LevelData =
  { blockLocation : (Int, Int)
  , currentLevel : Level
  }
